(function ($) {
	"use strict";

	var editMode = false;

	// All In One Widgets Start
	var JsExtandAllInOne = function ($scope, $) {

		var $slider = $scope.find('.extend-all-in-one-wrapper');

		if (!$slider.length) {
			return;
		}

		var $sliderContainer = $slider.find('.swiper-container'),
			$settings = $slider.data('carousel');
		// $Id              = $settings.id;


		var swiper = new Swiper($sliderContainer, $settings);

		if ($settings.pauseOnHover) {
			$($sliderContainer).hover(function () {
				(this).swiper.autoplay.stop();
			}, function () {
				(this).swiper.autoplay.start();
			});
		}

		// start video stop
		var stopVideos = function () {
			var videos = document.querySelectorAll($settings.id + ' .kit_extd-i-tabs-iframe');

			Array.prototype.forEach.call(videos, function (video) {
				var src = video.src;
				// video.src = src.replace("?autoplay=1", "");
				// video.src = src.replace("autoplay=1", "");
				video.src = src;
			});
		};
		// end video stop
		swiper.on('slideChange', function () {
			stopVideos();
		});

	};

	// All In One Widgets End

	$(window).on('elementor/frontend/init', function () {
		if (elementorFrontend.isEditMode()) {
			editMode = true;
		}

		elementorFrontend.hooks.addAction('frontend/element_ready/extend-all-in-one.default', JsExtandAllInOne);

	});

}(jQuery));

<?php
	
namespace ExtenderAllInOne\Includes\Traits;

use \Elementor\Controls_Manager;
use \Elementor\Group_Control_Background;
use \Elementor\Group_Control_Typography;
use \Elementor\Group_Control_Border;
use \Elementor\Group_Control_Box_Shadow;

defined( 'ABSPATH' ) || die();
	
trait Extender_Swiper_Controls {

		//Tab Section/Content Section Show Carousel Controls Global
		protected function extender_register_carousel_controls() {

            /**
            *  Content Tab Carousel Settings
            */
            $this->start_controls_section(
                'extend_section_carousel_settings',
                [
                    'label' => __( 'Carousel Settings - Global', 'extender-all-in-one-for-elementor' ),
                    'tab'   => Controls_Manager::TAB_CONTENT,
                ]
            );

            $slides_per_view = range( 1, 6 );
            $slides_per_view = array_combine( $slides_per_view, $slides_per_view );

            $this->add_responsive_control(
                'extend_carousel_slider_per_view',
                [
                    'type'    => Controls_Manager::SELECT,
                    'label'   => __( 'Columns', 'extender-all-in-one-for-elementor' ),
                    'options' => $slides_per_view,
                    'default' => '3',
                    'tablet_default' => '2',
                    'mobile_default' => '1',
                ]
            );

            $this->add_responsive_control(
                'extend_carousel_column_space',
                [
                    'label' => __( 'Column Space', 'extender-all-in-one-for-elementor' ),
                    'type' => Controls_Manager::SLIDER,
                    'size_units' => [ 'px', '%' ],
                    'range' => [
                        'px' => [
                            'min' => 0,
                            'max' => 30,
                        ],
                    ],
                    'default' => [
                        'unit' => 'px',
                        'size' => 15,
                    ]
                ]
            );

            $this->add_control(
                'extend_carousel_slides_per_column',
                [
                    'type'      => Controls_Manager::SELECT,
                    'label'     => __( 'Slides Per Column', 'extender-all-in-one-for-elementor' ),
                    'options'   => $slides_per_view,
                    'default'   => '1',
                ]
            );
            
            $this->add_control(
                'extend_carousel_transition_duration',
                [
                    'label'   => __( 'Transition Duration', 'extender-all-in-one-for-elementor' ),
                    'type'    => Controls_Manager::NUMBER,
                    'default' => 1000
                ]
            );

            $this->add_control(
                'extend_carousel_autoheight',
                [
                    'label'     => __( 'Auto Height', 'extender-all-in-one-for-elementor' ),
                    'type'      => Controls_Manager::SWITCHER,
                    'default'   => 'yes'
                ]
            );

            $this->add_control(
                'extend_carousel_autoplay',
                [
                    'label'     => __( 'Autoplay', 'extender-all-in-one-for-elementor' ),
                    'type'      => Controls_Manager::SWITCHER,
                    'default'   => 'yes'
                ]
            );

            $this->add_control(
                'extend_carousel_autoplay_speed',
                [
                    'label'     => __( 'Autoplay Speed', 'extender-all-in-one-for-elementor' ),
                    'type'      => Controls_Manager::NUMBER,
                    'default'   => 5000,
                    'condition' => [
                        'extend_carousel_autoplay' => 'yes'
                    ]
                ]
            );

            $this->add_control(
                'extend_carousel_loop',
                [
                    'label'   => __( 'Infinite Loop', 'extender-all-in-one-for-elementor' ),
                    'type'    => Controls_Manager::SWITCHER,
                    'default' => 'yes'
                ]
            );

            $this->add_control(
                'extend_carousel_pause',
                [
                    'label'     => esc_html__( 'Pause on Hover', 'extender-all-in-one-for-elementor' ),
                    'type'      => Controls_Manager::SWITCHER,
                    'default'   => 'yes',
                    'condition' => [
                        'extend_carousel_autoplay' => 'yes'
                    ]
                ]
            );

            $this->add_control(
                'extend_carousel_centered',
                [
                    'label'     => __( 'Centered Mode Slide', 'extender-all-in-one-for-elementor' ),
                    'type'      => Controls_Manager::SWITCHER,
                    'default'   => 'no',
                ]
            );
            
            $this->add_control(
                'extend_carousel_grab_cursor',
                [
                    'label'     => __( 'Grab Cursor', 'extender-all-in-one-for-elementor' ),
                    'type'      => Controls_Manager::SWITCHER,
                    'default'   => 'no',
                ]
            );

            $this->add_control(
                'extend_carousel_observer',
                [
                    'label'     => __( 'Observer', 'extender-all-in-one-for-elementor' ),
                    'type'      => Controls_Manager::SWITCHER,
                    'default'   => 'no',
                ]
            );

            $this->end_controls_section();
        }
     
		//Tab Section/Content Section Show Carousel Navigation Controls Global
		protected function extender_register_navigation_controls() {

            /**
            *  Content Tab Carousel navigation Settings
            */
            $this->start_controls_section(
                'extend_section_carousel_nav_settings',
                [
                    'label' => __( 'Navigation Settings - Global', 'extender-all-in-one-for-elementor' ),
                    'tab'   => Controls_Manager::TAB_CONTENT,
                ]
            );

            $this->add_control(
                'extend_carousel_nav',
                [
                    'label'   => __( 'Navigation Style', 'extender-all-in-one-for-elementor' ),
                    'type'    => Controls_Manager::SELECT,
                    'default' => 'arrows-dots',
                    'options' => [
                        'arrows'              => __( 'Arrows', 'extender-all-in-one-for-elementor' ),
                        'nav-dots'            => __( 'Dots', 'extender-all-in-one-for-elementor' ),
                        'arrows-dots'         => __( 'Arrows and Dots', 'extender-all-in-one-for-elementor' ),
                        'progress-bar'        => __( 'Progress Bar', 'extender-all-in-one-for-elementor' ),
                        'arrows-progress-bar' => __( 'Arrows and Progress Bar', 'extender-all-in-one-for-elementor' ),
                        'fraction'            => __( 'Fraction', 'extender-all-in-one-for-elementor' ),
                        'arrows-fraction'     => __( 'Arrows and Fraction', 'extender-all-in-one-for-elementor' ),
                        'none'                => __( 'None', 'extender-all-in-one-for-elementor' )                    
                    ]
                ]
            );

            $this->end_controls_section();
        
        }

        //Style Section Show Carousel Navigation Dots Style
		protected function extender_navigation_dots_style($id) {

            /**
            * Style Tab Dots Style
            */
            $this->start_controls_section(
                'extend_section_carousel_nav_dot',
                [
                    'label'     => esc_html__( 'Dots - Global', 'extender-all-in-one-for-elementor' ),
                    'tab'       => Controls_Manager::TAB_STYLE,
                    'condition' => [
                        'extend_carousel_nav' => ['nav-dots', 'arrows-dots'],
                    ],
                ]
            );

            $this->add_control(
                'extend_carousel_nav_dot_alignment',
                [
                    'label'   => __( 'Alignment', 'extender-all-in-one-for-elementor' ),
                    'type'    => Controls_Manager::CHOOSE,
                    'options' => [
                        'extand-carousel-dots-left'   => [
                            'title' => __( 'Left', 'extender-all-in-one-for-elementor' ),
                            'icon'  => 'eicon-h-align-left',
                        ],
                        'extand-carousel-dots-center' => [
                            'title' => __( 'Center', 'extender-all-in-one-for-elementor' ),
                            'icon'  => 'eicon-h-align-center',
                        ],
                        'extand-carousel-dots-right'  => [
                            'title' => __( 'Right', 'extender-all-in-one-for-elementor' ),
                            'icon'  => 'eicon-h-align-right',
                        ],
                    ],
                    'default' => 'extand-carousel-dots-center',
                ]
            );

            $this->add_responsive_control(
                'extend_carousel_nav_dots_top_spacing',
                [
                    'label'      => __( 'Top Spacing', 'extender-all-in-one-for-elementor' ),
                    'type'       => Controls_Manager::SLIDER,
                    'size_units' => ['px'],
                    'range'      => [
                        'px' => [
                            'min' => -200,
                            'max' => 200,
                        ],
                    ],
                    'default'    => [
                        'unit' => 'px',
                        'size' => 0,
                    ],
                    'selectors'  => [
                        '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination' => 'margin-top: {{SIZE}}{{UNIT}};',
                    ],
                ]
            );

            $this->add_responsive_control(
                'extend_carousel_nav_dots_spacing_btwn',
                [
                    'label' => __( 'Space Between', 'extender-all-in-one-for-elementor' ),
                    'type' => Controls_Manager::SLIDER,
                    'size_units' => [ 'px' ],
                    'range' => [
                        'px' => [
                            'min' => 0,
                            'max' => 100,
                        ],
                    ],
                    'default' => [
                        'unit' => 'px',
                        'size' => 8,
                    ],
                    'condition' => [
                        'extend_carousel_nav' => ['nav-dots', 'arrows-dots'],
                    ],
                    'selectors' => [
                        '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination .swiper-pagination-bullet:not(:last-child)' => 'margin-right: {{SIZE}}{{UNIT}};',
                    ]
                ]
            );

            $this->add_responsive_control(
                'extend_carousel_nav_dot_radius',
                [
                    'label'      => __( 'Border Radius', 'extender-all-in-one-for-elementor' ),
                    'type'       => Controls_Manager::DIMENSIONS,
                    'size_units' => ['px', '%'],
                    'default'    => [
                        'top'      => '0',
                        'right'    => '0',
                        'bottom'   => '0',
                        'left'     => '0',
                        'unit'     => 'px',
                        'isLinked' => true,
                    ],
                    'selectors'  => [
                        '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination .swiper-pagination-bullet' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                    ],
                ]
            );

            $this->start_controls_tabs( 'extend_carousel_nav_dots_tabs' );

                // normal state rating
                $this->start_controls_tab( 'extend_carousel_nav_dots_normal', [ 'label' => esc_html__( 'Normal', 'extender-all-in-one-for-elementor' ) ] );

                    $this->add_responsive_control(
                        'extend_carousel_dots_normal_height',
                        [
                            'label'      => __( 'Height', 'extender-all-in-one-for-elementor' ),
                            'type'       => Controls_Manager::SLIDER,
                            'size_units' => ['px'],
                            'range'      => [
                                'px' => [
                                    'min' => 0,
                                    'max' => 100,
                                ],
                            ],
                            'default'    => [
                                'unit' => 'px',
                                'size' => 10,
                            ],
                            'selectors'  => [
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination .swiper-pagination-bullet' => 'height: {{SIZE}}{{UNIT}};',
                            ],
                        ]
                    );

                    $this->add_responsive_control(
                        'extend_carousel_dots_normal_width',
                        [
                            'label'      => __( 'Width', 'extender-all-in-one-for-elementor' ),
                            'type'       => Controls_Manager::SLIDER,
                            'size_units' => ['px'],
                            'range'      => [
                                'px' => [
                                    'min' => 0,
                                    'max' => 100,
                                ],
                            ],
                            'default'    => [
                                'unit' => 'px',
                                'size' => 10,
                            ],
                            'selectors'  => [
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination .swiper-pagination-bullet' => 'width: {{SIZE}}{{UNIT}};',
                            ],
                        ]
                    );

                    $this->add_control(
                        'extend_carousel_dots_normal_background',
                        [
                            'label'     => __( 'Background Color', 'extender-all-in-one-for-elementor' ),
                            'type'      => Controls_Manager::COLOR,
                            'default'   => '#e5e5e5',
                            'selectors' => [
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination .swiper-pagination-bullet' => 'background: {{VALUE}};',
                            ],
                        ]
                    );
                    
                    $this->add_responsive_control(
                        'extend_carousel_dots_normal_opacity',
                        [
                            'label'     => __( 'Opacity', 'extender-all-in-one-for-elementor' ),
                            'type'      => Controls_Manager::SLIDER,
                            'default' => [
                                'size' => 1,
                            ],
                            'range' => [
                                'px' => [
                                    'max' => 1,
                                    'step' => 0.01,
                                ],
                            ],
                            'selectors' => [
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination .swiper-pagination-bullet' => 'opacity: {{SIZE}};',
                            ],
                        ]
                    );

                    $this->add_group_control(
                        Group_Control_Border::get_type(),
                        [
                            'name'     => 'extend_carousel_dots_normal_border',
                            'label'    => __( 'Border', 'extender-all-in-one-for-elementor' ),
                            'selector' => '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination .swiper-pagination-bullet , {{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination .swiper-pagination-bullet:hover',
                        ]
                    );

                $this->end_controls_tab();

                // hover state rating
                $this->start_controls_tab( 'extend_carousel_nav_dots_hover', [ 'label' => esc_html__( 'Hover/active', 'extender-all-in-one-for-elementor' ) ] );
                
                    $this->add_responsive_control(
                        'extend_carousel_dots_active_height',
                        [
                            'label'      => __( 'Height', 'extender-all-in-one-for-elementor' ),
                            'type'       => Controls_Manager::SLIDER,
                            'size_units' => ['px', '%'],
                            'range'      => [
                                'px' => [
                                    'min' => 0,
                                    'max' => 100,
                                ],
                            ],
                            'default'    => [
                                'unit' => 'px',
                                'size' => 10,
                            ],
                            'selectors'  => [
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination .swiper-pagination-bullet-active' => 'height: {{SIZE}}{{UNIT}};',
                            ],
                        ]
                    );

                    $this->add_responsive_control(
                        'extend_carousel_dots_active_width',
                        [
                            'label'      => __( 'Width', 'extender-all-in-one-for-elementor' ),
                            'type'       => Controls_Manager::SLIDER,
                            'size_units' => ['px', '%'],
                            'range'      => [
                                'px' => [
                                    'min' => 0,
                                    'max' => 100,
                                ],
                            ],
                            'default'    => [
                                'unit' => 'px',
                                'size' => 10,
                            ],
                            'selectors'  => [
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination .swiper-pagination-bullet-active' => 'width: {{SIZE}}{{UNIT}};',
                            ],
                        ]
                    );

                    $this->add_control(
                        'extend_carousel_dots_active_background',
                        [
                            'label'     => __( 'Background Color', 'extender-all-in-one-for-elementor' ),
                            'type'      => Controls_Manager::COLOR,
                            'default'   => "#fd71af",
                            'selectors' => [
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination .swiper-pagination-bullet-active' => 'background: {{VALUE}};',
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination .swiper-pagination-bullet:hover' => 'background: {{VALUE}};',
                            ],
                        ]
                    );

                    $this->add_responsive_control(
                        'extend_carousel_carousel_dots_hover_opacity',
                        [
                            'label'     => __( 'Opacity', 'extender-all-in-one-for-elementor' ),
                            'type'      => Controls_Manager::SLIDER,
                            'default' => [
                                'size' => 1,
                            ],
                            'range' => [
                                'px' => [
                                    'max' => 1,
                                    'step' => 0.01,
                                ],
                            ],
                            'selectors' => [
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination .swiper-pagination-bullet-active' => 'opacity: {{SIZE}};',
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination .swiper-pagination-bullet:hover' => 'opacity: {{SIZE}};',
                            ],
                        ]
                    );

                    $this->add_group_control(
                        Group_Control_Border::get_type(),
                        [
                            'name'     => 'extend_carousel_dots_active_border',
                            'label'    => __( 'Border', 'extender-all-in-one-for-elementor' ),
                            'selector' => '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination .swiper-pagination-bullet-active, {{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination .swiper-pagination-bullet:hover',
                        ]
                    );

                $this->end_controls_tab();

            $this->end_controls_tabs();

            $this->end_controls_section();
        }

        //Style Section Show Carousel Navigation Arrows Style
		protected function extender_navigation_arrows_style($id) {

            
            /**
             * Style Tab Arrows Style
             */
            $this->start_controls_section(
                'extend_carousel_nav_arrow',
                [
                    'label'     => esc_html__( 'Arrows - Global', 'extender-all-in-one-for-elementor' ),
                    'tab'       => Controls_Manager::TAB_STYLE,
                    'condition' => [
                        'extend_carousel_nav' => ['arrows', 'arrows-dots', 'arrows-fraction', 'arrows-progress-bar'],
                    ],
                ]
            );

            $this->add_responsive_control(
                'extend_carousel_nav_arrow_box_size',
                [
                    'label'      => __( 'Box Size', 'extender-all-in-one-for-elementor' ),
                    'type'       => Controls_Manager::SLIDER,
                    'size_units' => ['px'],
                    'range'      => [
                        'px' => [
                            'min' => 0,
                            'max' => 200,
                        ],
                    ],
                    'default'    => [
                        'unit' => 'px',
                        'size' => 50,
                    ],
                    'selectors'  => [
                        '{{WRAPPER}} .extand-carousel-wrapper .extand-carousel-nav-prev' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
                        '{{WRAPPER}} .extand-carousel-wrapper .extand-carousel-nav-next' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
                    ],
                ]
            );

            $this->add_responsive_control(
                'extend_carousel_nav_arrow_icon_size',
                [
                    'label'      => __( 'Icon Size', 'extender-all-in-one-for-elementor' ),
                    'type'       => Controls_Manager::SLIDER,
                    'size_units' => ['px'],
                    'range'      => [
                        'px' => [
                            'min' => 0,
                            'max' => 50,
                        ],
                    ],
                    'default'    => [
                        'unit' => 'px',
                        'size' => 16,
                    ],
                    'selectors'  => [
                        '{{WRAPPER}} .extand-carousel-wrapper .extand-carousel-nav-prev i' => 'font-size: {{SIZE}}{{UNIT}};',
                        '{{WRAPPER}} .extand-carousel-wrapper .extand-carousel-nav-next i' => 'font-size: {{SIZE}}{{UNIT}};',
                    ],
                ]
            );
            
            $this->add_control(
                'extend_carousel_prev_arrow_position',
                [
                    'label'        => __( 'Previous Arrow Position', 'extender-all-in-one-for-elementor' ),
                    'type'         => Controls_Manager::POPOVER_TOGGLE,
                    'label_off'    => __( 'Default', 'extender-all-in-one-for-elementor' ),
                    'label_on'     => __( 'Custom', 'extender-all-in-one-for-elementor' ),
                    'return_value' => 'yes',
                    'default'      => 'yes',
                ]
            );
            
            $this->start_popover();

                $this->add_responsive_control(
                    'extend_carousel_prev_arrow_position_x_offset',
                    [
                        'label'      => __( 'X Offset', 'extender-all-in-one-for-elementor' ),
                        'type'       => Controls_Manager::SLIDER,
                        'size_units' => ['px', '%'],
                        'range'      => [
                            'px' => [
                                'min' => -3000,
                                'max' => 3000,
                            ],
                            '%'  => [
                                'min' => -100,
                                'max' => 100,
                            ],
                        ],
                        'default'    => [
                            'unit' => 'px',
                            'size' => 30,
                        ],
                        'selectors'  => [
                            '{{WRAPPER}} .extand-carousel-wrapper .extand-carousel-nav-prev' => 'left: {{SIZE}}{{UNIT}};',
                        ],
                    ]
                );

                $this->add_responsive_control(
                    'extend_carousel_prev_arrow_position_y_offset',
                    [
                        'label'      => __( 'Y Offset', 'extender-all-in-one-for-elementor' ),
                        'type'       => Controls_Manager::SLIDER,
                        'size_units' => ['px', '%'],
                        'range'      => [
                            'px' => [
                                'min' => -3000,
                                'max' => 3000,
                            ],
                            '%'  => [
                                'min' => -100,
                                'max' => 100,
                            ],
                        ],
                        'default'    => [
                            'unit' => '%',
                            'size' => 50,
                        ],
                        'selectors'  => [
                            '{{WRAPPER}} .extand-carousel-wrapper .extand-carousel-nav-prev' => 'top: {{SIZE}}{{UNIT}};',
                        ],
                    ]
                );

            $this->end_popover();

            $this->add_control(
                'extend_carousel_next_arrow_position',
                [
                    'label'        => __( 'Next Arrow Position', 'extender-all-in-one-for-elementor' ),
                    'type'         => Controls_Manager::POPOVER_TOGGLE,
                    'label_off'    => __( 'Default', 'extender-all-in-one-for-elementor' ),
                    'label_on'     => __( 'Custom', 'extender-all-in-one-for-elementor' ),
                    'return_value' => 'yes',
                    'default'      => 'yes',
                ]
            );
            
            $this->start_popover();

                $this->add_responsive_control(
                    'extend_carousel_next_arrow_position_x_offset',
                    [
                        'label'      => __( 'X Offset', 'extender-all-in-one-for-elementor' ),
                        'type'       => Controls_Manager::SLIDER,
                        'size_units' => ['px', '%'],
                        'range'      => [
                            'px' => [
                                'min' => -3000,
                                'max' => 3000,
                            ],
                            '%'  => [
                                'min' => -100,
                                'max' => 100,
                            ],
                        ],
                        'default'    => [
                            'unit' => 'px',
                            'size' => 30,
                        ],
                        'selectors'  => [
                            '{{WRAPPER}} .extand-carousel-wrapper .extand-carousel-nav-next' => 'right: {{SIZE}}{{UNIT}};',
                        ],
                    ]
                );

                $this->add_responsive_control(
                    'extend_carousel_next_arrow_position_y_offset',
                    [
                        'label'      => __( 'Y Offset', 'extender-all-in-one-for-elementor' ),
                        'type'       => Controls_Manager::SLIDER,
                        'size_units' => ['px', '%'],
                        'range'      => [
                            'px' => [
                                'min' => -3000,
                                'max' => 3000,
                            ],
                            '%'  => [
                                'min' => -100,
                                'max' => 100,
                            ],
                        ],
                        'default'    => [
                            'unit' => '%',
                            'size' => 50,
                        ],
                        'selectors'  => [
                            '{{WRAPPER}} .extand-carousel-wrapper .extand-carousel-nav-next' => 'top: {{SIZE}}{{UNIT}};',
                        ],
                    ]
                );

            $this->end_popover();

            $this->add_responsive_control(
                'extend_carousel_nav_arrow_radius',
                [
                    'label'      => __( 'Border radius', 'extender-all-in-one-for-elementor' ),
                    'type'       => Controls_Manager::DIMENSIONS,
                    'size_units' => ['px', '%'],
                    'default'    => [
                        'top'      => '50',
                        'right'    => '50',
                        'bottom'   => '50',
                        'left'     => '50',
                        'unit'     => 'px',
                        'isLinked' => true,
                    ],
                    'selectors'  => [
                        '{{WRAPPER}} .extand-carousel-wrapper .extand-carousel-nav-prev' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                        '{{WRAPPER}} .extand-carousel-wrapper .extand-carousel-nav-next' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                    ],
                ]
            );

            $this->start_controls_tabs( 'extend_carousel_nav_arrow_tabs' );

                // normal state rating
                $this->start_controls_tab( 'extend_carousel_nav_arrow_normal', [ 'label' => esc_html__( 'Normal', 'extender-all-in-one-for-elementor' ) ] );

                    $this->add_control(
                        'extend_carousel_arrow_normal_background',
                        [
                            'label'     => __( 'Background Color', 'extender-all-in-one-for-elementor' ),
                            'type'      => Controls_Manager::COLOR,
                            'default'   => '#fd71af',
                            'selectors' => [
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-carousel-nav-prev' => 'background: {{VALUE}}',
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-carousel-nav-next' => 'background: {{VALUE}}',
                            ],
                        ]
                    );

                    $this->add_control(
                        'extend_carousel_arrow_normal_color',
                        [
                            'label'     => __( 'Icon Color', 'extender-all-in-one-for-elementor' ),
                            'type'      => Controls_Manager::COLOR,
                            'default'   => '#ffffff',
                            'selectors' => [
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-carousel-nav-prev i' => 'color: {{VALUE}};',
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-carousel-nav-next i' => 'color: {{VALUE}};',
                            ],
                        ]
                    );

                    $this->add_group_control(
                        Group_Control_Border::get_type(),
                        [
                            'name'     => 'extend_carousel_arrow_normal_border',
                            'label'    => __( 'Border', 'extender-all-in-one-for-elementor' ),
                            'selector' => '{{WRAPPER}} .extand-carousel-wrapper .extand-carousel-nav-prev, {{WRAPPER}} .extand-carousel-wrapper .extand-carousel-nav-next',
                        ]
                    );

                    $this->add_group_control(
                        Group_Control_Box_Shadow::get_type(),
                        [
                            'name'     => 'extend_carousel_arrow_normal_shadow',
                            'label'    => __( 'Box Shadow', 'extender-all-in-one-for-elementor' ),
                            'selector' => '{{WRAPPER}} .extand-carousel-wrapper .extand-carousel-nav-prev, {{WRAPPER}} .extand-carousel-wrapper .extand-carousel-nav-next',
                        ]
                    );

                $this->end_controls_tab();

                // hover state rating
                $this->start_controls_tab( 'extend_carousel_nav_arrow_hover', [ 'label' => esc_html__( 'Hover', 'extender-all-in-one-for-elementor' ) ] );

                    $this->add_control(
                        'extend_carousel_arrow_hover_background',
                        [
                            'label'     => __( 'Background Color', 'extender-all-in-one-for-elementor' ),
                            'type'      => Controls_Manager::COLOR,
                            'default'   => '#fd71af',
                            'selectors' => [
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-carousel-nav-prev:hover' => 'background: {{VALUE}}',
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-carousel-nav-next:hover' => 'background: {{VALUE}}',
                            ],
                        ]
                    );

                    $this->add_control(
                        'extend_carousel_arrow_hover_color',
                        [
                            'label'     => __( 'Icon Color', 'extender-all-in-one-for-elementor' ),
                            'type'      => Controls_Manager::COLOR,
                            'default'   => '#ffffff',
                            'selectors' => [
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-carousel-nav-prev:hover i' => 'color: {{VALUE}}',
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-carousel-nav-next:hover i' => 'color: {{VALUE}}',
                            ],
                        ]
                    );

                    $this->add_group_control(
                        Group_Control_Border::get_type(),
                        [
                            'name'     => 'extend_carousel_arrow_hover_border',
                            'label'    => __( 'Border', 'extender-all-in-one-for-elementor' ),
                            'selector' => '{{WRAPPER}} .extand-carousel-wrapper .extand-carousel-nav-prev:hover, {{WRAPPER}} .extand-carousel-wrapper .extand-carousel-nav-next:hover',
                        ]
                    );

                    $this->add_group_control(
                        Group_Control_Box_Shadow::get_type(),
                        [
                            'name'     => 'extend_carousel_arrow_hover_shadow',
                            'label'    => __( 'Box Shadow', 'extender-all-in-one-for-elementor' ),
                            'selector' => '{{WRAPPER}} .extand-carousel-wrapper .extand-carousel-nav-prev:hover, {{WRAPPER}} .extand-carousel-wrapper .extand-carousel-nav-next:hover',
                        ]
                    );

                $this->end_controls_tab();

            $this->end_controls_tabs();

            $this->end_controls_section();

        }

        //Style Section Show Carousel fraction Arrows Style
		protected function extender_navigation_fraction_style($id) {

            $this->start_controls_section(
                'extend_carousel_nav_fraction',
                [
                    'label'     => esc_html__( 'Fraction - Global', 'extender-all-in-one-for-elementor' ),
                    'tab'       => Controls_Manager::TAB_STYLE,
                    'condition' => [
                        'extend_carousel_nav' => ['fraction', 'arrows-fraction'],
                    ],
                ]
            );
    
            $this->add_control(
                'extend_carousel_nav_fraction_alignment',
                [
                    'label'   => __( 'Alignment', 'extender-all-in-one-for-elementor' ),
                    'type'    => Controls_Manager::CHOOSE,
                    'options' => [
                        'extand-carousel-dots-left'   => [
                            'title' => __( 'Left', 'extender-all-in-one-for-elementor' ),
                            'icon'  => 'fa fa-align-left',
                        ],
                        'extand-carousel-dots-center' => [
                            'title' => __( 'Center', 'extender-all-in-one-for-elementor' ),
                            'icon'  => 'fa fa-align-center',
                        ],
                        'extand-carousel-dots-right'  => [
                            'title' => __( 'Right', 'extender-all-in-one-for-elementor' ),
                            'icon'  => 'fa fa-align-right',
                        ],
                    ],
                    'default' => 'extand-carousel-dots-center',
                ]
            );
    
            $this->add_responsive_control(
                'extend_carousel_fraction_top_spacing',
                [
                    'label'      => __( 'Top Spacing', 'extender-all-in-one-for-elementor' ),
                    'type'       => Controls_Manager::SLIDER,
                    'size_units' => ['px'],
                    'range'      => [
                        'px' => [
                            'min' => -200,
                            'max' => 200,
                        ],
                    ],
                    'default'    => [
                        'unit' => 'px',
                        'size' => 30,
                    ],
                    'selectors'  => [
                        '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination' => 'margin-top: {{SIZE}}{{UNIT}};',
                    ],
                ]
            );
    
            $this->add_responsive_control(
                'extend_carousel_fraction_spacing_btwn',
                [
                    'label' => __( 'Space Between', 'extender-all-in-one-for-elementor' ),
                    'type' => Controls_Manager::SLIDER,
                    'size_units' => [ 'px' ],
                    'range' => [
                        'px' => [
                            'min' => 0,
                            'max' => 100,
                        ],
                    ],
                    'default' => [
                        'unit' => 'px',
                        'size' => 8,
                    ],
                    'selectors' => [
                        '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination.swiper-pagination-fraction .swiper-pagination-current' => 'margin-right: {{SIZE}}{{UNIT}};',
                        '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination.swiper-pagination-fraction .swiper-pagination-total' => 'margin-left: {{SIZE}}{{UNIT}};',
                    ]
                ]
            );
    
            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name'     => 'extend_carousel_swiper-pagination_fraction_typography',
                    'selector' => '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination.swiper-pagination-fraction *',
                ]
            );
    
            $this->add_responsive_control(
                'extend_carousel_nav_fraction_radius',
                [
                    'label'      => __( 'Border Radius', 'extender-all-in-one-for-elementor' ),
                    'type'       => Controls_Manager::DIMENSIONS,
                    'size_units' => ['px', '%'],
                    'default'    => [
                        'top'      => '50',
                        'right'    => '50',
                        'bottom'   => '50',
                        'left'     => '50',
                        'unit'     => 'px',
                        'isLinked' => true,
                    ],
                    'selectors'  => [
                        '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination.swiper-pagination-fraction span' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                    ],
                ]
            );
    
            $this->start_controls_tabs( 'extend_carousel_nav_fraction_tabs' );
            // normal state rating
    
                $this->start_controls_tab( 'extend_carousel_nav_fraction_normal', [ 'label' => esc_html__( 'Normal', 'extender-all-in-one-for-elementor' ) ] );
                    $this->add_responsive_control(
                        'extend_carousel_fraction_normal_height',
                        [
                            'label'      => __( 'Height', 'extender-all-in-one-for-elementor' ),
                            'type'       => Controls_Manager::SLIDER,
                            'size_units' => ['px'],
                            'range'      => [
                                'px' => [
                                    'min' => 0,
                                    'max' => 100,
                                ],
                            ],
                            'default'    => [
                                'unit' => 'px',
                                'size' => 32,
                            ],
                            'selectors'  => [
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination.swiper-pagination-fraction .swiper-pagination-total' => 'height: {{SIZE}}{{UNIT}};',
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination.swiper-pagination-fraction .swiper-pagination-total' => 'line-height: {{SIZE}}{{UNIT}};',
                            ],
                        ]
                    );
    
                    $this->add_responsive_control(
                        'extend_carousel_fraction_normal_width',
                        [
                            'label'      => __( 'Width', 'extender-all-in-one-for-elementor' ),
                            'type'       => Controls_Manager::SLIDER,
                            'size_units' => ['px'],
                            'range'      => [
                                'px' => [
                                    'min' => 0,
                                    'max' => 100,
                                ],
                            ],
                            'default'    => [
                                'unit' => 'px',
                                'size' => 32,
                            ],
                            'selectors'  => [
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination.swiper-pagination-fraction .swiper-pagination-total' => 'width: {{SIZE}}{{UNIT}};',
                            ],
                        ]
                    );
    
                    $this->add_control(
                        'extend_carousel_pagination_fraction_color',
                        [
                            'label'     => esc_html__( 'Color', 'extender-all-in-one-for-elementor' ),
                            'type'      => Controls_Manager::COLOR,
                            'default'   => '#000000',
                            'selectors' => [
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination.swiper-pagination-fraction .swiper-pagination-total' => 'color: {{VALUE}};'
                            ]
                        ]
                    );      
                    
                    $this->add_control(
                        'extend_carousel_fraction_normal_background',
                        [
                            'label'     => __( 'Background Color', 'extender-all-in-one-for-elementor' ),
                            'type'      => Controls_Manager::COLOR,
                            'default'   => '#e5e5e5',
                            'selectors' => [
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination.swiper-pagination-fraction .swiper-pagination-total' => 'background: {{VALUE}};',
                            ],
                        ]
                    );
                    
                    $this->add_group_control(
                        Group_Control_Border::get_type(),
                        [
                            'name'     => 'extend_carousel_fraction_normal_border',
                            'label'    => __( 'Border', 'extender-all-in-one-for-elementor' ),
                            'selector' => '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination.swiper-pagination-fraction .swiper-pagination-total',
                        ]
                    );
                $this->end_controls_tab();
    
                // hover state rating
                $this->start_controls_tab( 'extend_carousel_nav_fraction_hover', [ 'label' => esc_html__( 'Hover/active', 'extender-all-in-one-for-elementor' ) ] );
                
                    $this->add_responsive_control(
                        'extend_carousel_fraction_active_height',
                        [
                            'label'      => __( 'Height', 'extender-all-in-one-for-elementor' ),
                            'type'       => Controls_Manager::SLIDER,
                            'size_units' => ['px', '%'],
                            'range'      => [
                                'px' => [
                                    'min' => 0,
                                    'max' => 100,
                                ],
                            ],
                            'default'    => [
                                'unit' => 'px',
                                'size' => 32,
                            ],
                            'selectors'  => [
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination.swiper-pagination-fraction .swiper-pagination-current' => 'height: {{SIZE}}{{UNIT}};',
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination.swiper-pagination-fraction .swiper-pagination-current' => 'line-height: {{SIZE}}{{UNIT}};',
                            ],
                        ]
                    );
    
                    $this->add_responsive_control(
                        'extend_carousel_fraction_active_width',
                        [
                            'label'      => __( 'Width', 'extender-all-in-one-for-elementor' ),
                            'type'       => Controls_Manager::SLIDER,
                            'size_units' => ['px', '%'],
                            'range'      => [
                                'px' => [
                                    'min' => 0,
                                    'max' => 100,
                                ],
                            ],
                            'default'    => [
                                'unit' => 'px',
                                'size' => 32,
                            ],
                            'selectors'  => [
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination.swiper-pagination-fraction .swiper-pagination-current' => 'width: {{SIZE}}{{UNIT}};',
                            ],
                        ]
                    );
    
                    $this->add_control(
                        'extend_carousel_pagination_fraction_current_color',
                        [
                            'label'     => esc_html__( 'Color', 'extender-all-in-one-for-elementor' ),
                            'type'      => Controls_Manager::COLOR,
                            'default'   => '#fff',
                            'selectors' => [
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination.swiper-pagination-fraction .swiper-pagination-current' => 'color: {{VALUE}};'
                            ]
                        ]
                    );     
    
                    $this->add_control(
                        'extend_carousel_fraction_active_background',
                        [
                            'label'     => __( 'Background Color', 'extender-all-in-one-for-elementor' ),
                            'type'      => Controls_Manager::COLOR,
                            'default'   => '#fd71af',
                            'selectors' => [
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination.swiper-pagination-fraction .swiper-pagination-current:hover' => 'background: {{VALUE}};',
                                '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination.swiper-pagination-fraction .swiper-pagination-current' => 'background: {{VALUE}};',
                            ],
                        ]
                    );
    
                    $this->add_group_control(
                        Group_Control_Border::get_type(),
                        [
                            'name'     => 'extend_carousel_fraction_active_border',
                            'label'    => __( 'Border', 'extender-all-in-one-for-elementor' ),
                            'selector' => '{{WRAPPER}} .extand-carousel-wrapper .extand-swiper-pagination.swiper-pagination-fraction .swiper-pagination-current',
                        ]
                    );
    
                $this->end_controls_tab();
            $this->end_controls_tabs();
            $this->end_controls_section();
    
        }

        //Style Section Show Carousel fraction Arrows Style
		protected function extender_navigation_progressbar_style($id) {
            $this->start_controls_section(
                'extend_carousel_nav_progressbar',
                [
                    'label'     => esc_html__( 'Progress Bar - Global', 'extender-all-in-one-for-elementor' ),
                    'tab'       => Controls_Manager::TAB_STYLE,
                    'condition' => [
                        'extend_carousel_nav' => ['progress-bar', 'arrows-progress-bar'],
                    ],
                ]
            );
    
            $this->add_control(
                'extend_carousel_nav_progressbar_normal_color',
                [
                    'label'     => __( 'Normal Color', 'extender-all-in-one-for-elementor' ),
                    'type'      => Controls_Manager::COLOR,
                    'default'   => '#e5e5e5',
                    'selectors' => [
                        '{{WRAPPER}} .extand-carousel-wrapper .extand-dots-container .extand-swiper-pagination.swiper-pagination.swiper-pagination-progressbar' => 'background: {{VALUE}};',
                    ],
                ]
            );
            
            $this->add_control(
                'extend_carousel_nav_progressbar_active_color',
                [
                    'label'     => __( 'Active Color', 'extender-all-in-one-for-elementor' ),
                    'type'      => Controls_Manager::COLOR,
                    'default'   => '#fd71af',
                    'separator' => "after",
                    'selectors' => [
                        '{{WRAPPER}} .extand-carousel-wrapper .extand-dots-container .extand-swiper-pagination.swiper-pagination.swiper-pagination-progressbar .swiper-pagination-progressbar-fill' => 'background: {{VALUE}};',
                    ],
                ]
            );
    
            $this->add_control(
                'extend_carousel_nav_Progress_position',
                [
                    'label'   => __( 'Position', 'extender-all-in-one-for-elementor' ),
                    'type'    => Controls_Manager::CHOOSE,
                    'toggle'  => false,
                    'default' => 'extand-Progressbar-align-top',
                    'options' => [
                        'extand-Progressbar-align-top' => [
                            'title' => __( 'Top', 'extender-all-in-one-for-elementor' ),
                            'icon'  => 'eicon-arrow-up'
                        ],
                        'extand-Progressbar-align-bottom' => [
                            'title' => __( 'Bottom', 'extender-all-in-one-for-elementor' ),
                            'icon'  => 'eicon-arrow-down'
                        ]
                    ]
                ]
            );
            
            $this->add_responsive_control(
                'extend_carousel_nav_Progress_specing',
                [
                    'label'       => __( 'Spacing', 'extender-all-in-one-for-elementor' ),
                    'type'        => Controls_Manager::SLIDER,
                    'size_units'  => [ 'px' ],
                    'range'       => [
                        'px'      => [
                            'min' => 0,
                            'max' => 100
                        ]
                    ],
                    'default'     => [
                        'unit'    => 'px',
                        'size'    => 0
                    ],
                    'selectors'   => [
                        '{{WRAPPER}} .extand-carousel-wrapper.extand-carousel-item.extand-Progressbar-align-top .extand-dots-container .extand-swiper-pagination.swiper-pagination.swiper-pagination-progressbar' => 'top: {{SIZE}}{{UNIT}};',
                        '{{WRAPPER}} .extand-carousel-wrapper.extand-carousel-item.extand-Progressbar-align-bottom .extand-dots-container .extand-swiper-pagination.swiper-pagination.swiper-pagination-progressbar' => 'bottom: {{SIZE}}{{UNIT}};'
                    ]
                ]
            );
    
            $this->add_responsive_control(
                'extend_carousel_nav_progressbar_height',
                [
                    'label'      => __( 'Height', 'extender-all-in-one-for-elementor' ),
                    'type'       => Controls_Manager::SLIDER,
                    'size_units' => ['px'],
                    'range'      => [
                        'px' => [
                            'min' => 0,
                            'max' => 10,
                        ],
                    ],
                    'default'    => [
                        'unit' => 'px',
                        'size' => 4,
                    ],
                    'selectors'  => [
                        '{{WRAPPER}} .extand-carousel-wrapper .extand-dots-container .extand-swiper-pagination.swiper-pagination.swiper-pagination-progressbar' => 'height: {{SIZE}}{{UNIT}};',
                    ],
                ]
            );
    
            $this->end_controls_section();

        }


         //Render Carousel Arg or Parametter
		protected function extender_render_global_swiper_attribute($id) {

            $settings    = $this->get_settings_for_display();
            $carousel_id = 'extand-carousel-' . $this->get_id();
    
            $this->add_render_attribute(
                'extend-carousel-wrapper',
                [
                    'id'    => $carousel_id,
                    'class' => "extand-carousel-wrapper"
                ]
            );
    
            // Carousel Settings
            $elementor_viewport_lg = get_option( 'elementor_viewport_lg' );
            $elementor_viewport_md = get_option( 'elementor_viewport_md' );
            $extend_viewport_lg     = !empty($elementor_viewport_lg) ? $elementor_viewport_lg - 1 : 1023;
            $extend_viewport_md     = !empty($elementor_viewport_md) ? $elementor_viewport_md - 1 : 767;

            if ( 'nav-dots' === $settings['extend_carousel_nav'] || 'arrows-dots' === $settings['extend_carousel_nav'] ) {
                $swiper_pagination_type = 'bullets';
            } elseif ( 'fraction' === $settings['extend_carousel_nav'] || 'arrows-fraction' === $settings['extend_carousel_nav'] ) {
                $swiper_pagination_type = 'fraction';
            } elseif ( 'progress-bar' === $settings['extend_carousel_nav'] || 'arrows-progress-bar' === $settings['extend_carousel_nav'] ) {
                $swiper_pagination_type = 'progressbar';
            } else {
                $swiper_pagination_type = '';
            } 

            $carousel_data_settings = wp_json_encode(
                array_filter([
                    "id"                    => $carousel_id,
                    "autoplay"           	=> $settings["extend_carousel_autoplay"] ? true : false,
                    "delay" 				=> $settings["extend_carousel_autoplay_speed"] ? true : false,
                    "loop"           		=> $settings["extend_carousel_loop"] ? true : false,
                    "speed"       			=> $settings["extend_carousel_transition_duration"],
                    "pauseOnHover"       	=> $settings["extend_carousel_pause"] ? true : false,
                    "slidesPerView"         => isset( $settings["extend_carousel_slider_per_view_mobile"] ) ? (int) $settings["extend_carousel_slider_per_view_mobile"] : 1,
                    "slidesPerColumn" 		=> ($settings["extend_carousel_slides_per_column"] > 1) ? $settings["extend_carousel_slides_per_column"] : false,
                    "centeredSlides"        => ( $settings["extend_carousel_centered"] === "yes" ) ? true : false,
                    "spaceBetween"   		=> $settings['extend_carousel_column_space']['size'],
                    "grabCursor"  			=> ($settings["extend_carousel_grab_cursor"] === "yes") ? true : false,
                    "observer"       		=> ($settings["extend_carousel_observer"]) ? true : false,
                    "observeParents" 		=> ($settings["extend_carousel_observer"]) ? true : false,
                    "breakpoints"     		=> [
    
                        (int) $extend_viewport_md 	=> [
                            "slidesPerView" 	=> isset( $settings["extend_carousel_slider_per_view_tablet"] ) ? (int) $settings["extend_carousel_slider_per_view_tablet"] : 2,
                            "spaceBetween"  	=> $settings["extend_carousel_column_space"]["size"],
                            
                        ],
                        (int) $extend_viewport_lg 	=> [
                            "slidesPerView" 	=> (int) $settings["extend_carousel_slider_per_view"],
                            "spaceBetween"  	=> $settings["extend_carousel_column_space"]["size"],
                            
                        ]
                    ],
                    "pagination" 			 	=>  [ 
                        "el" 				=> "#". $carousel_id . " .extand-swiper-pagination",
                        "type"       		=> $swiper_pagination_type,
                        "clickable"  		=> true,
                    ],
                    "navigation" => [
                        "nextEl" => "#". $carousel_id . " .extand-carousel-nav-next",
                        "prevEl" => "#". $carousel_id . " .extand-carousel-nav-prev",
                    ],
    
                ])
            );
            $this->add_render_attribute( 'extend-carousel-wrapper', 'data-carousel',  $carousel_data_settings );
            $this->add_render_attribute( 'extend-carousel-wrapper', 'class', esc_attr( $settings['extend_carousel_nav_dot_alignment'] ) );
            $this->add_render_attribute( 'extend-carousel-wrapper', 'class', esc_attr( $settings['extend_carousel_nav_fraction_alignment'] ) );
            if ( 'progress-bar' == $settings['extend_carousel_nav'] || 'arrows-progress-bar' == $settings['extend_carousel_nav']) {
                $this->add_render_attribute( 'extend-carousel-wrapper', 'class', esc_attr( $settings['extend_carousel_nav_Progress_position'] ) );
            }
    
            // Carousel Settings end
    
        }
}
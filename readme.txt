===  Extender All In One For Elementor ===
Contributors: abdullahtaf
Author URI: https://profiles.wordpress.org/abdullahtaf/
Tags: elementor, elements, addons, elementor addon, elementor widget, page builder, builder, visual editor, wordpress page builder, service info carousel, icon-carousel, extender all in one, trendy
Requires at least: 5.0.0
Tested up to: 5.8.1
Requires PHP: 5.6
Stable tag: 1.0.2
License: GPLv3
License URI: https://opensource.org/licenses/GPL-3.0

Extender All In One For Elementor comes with one widgets and extensions to extend the power of Elementor Page Builder. 

== Description ==

The most modern and trendy widget. 14 types of design you can make by all in one widget.

1. Image services with carousel
2. Image services with grid
3. Icon services with carousel
4. Icon services with grid
5. Icon box with carousel
6. Icon box with grid
7. Logo carousel with external link
8. Logo grid with with external link
9. Image carousel with external link
10. Image grid with with external link
11. ShortCode grid
12. ShortCode carousel
13. Template carousel
14. Template grid

== Check Demos ==

Demo Link : [https://crazydevs.xyz/crazy-devs/widgets-demo/]

### Customizable

You can customize these elements almost every possible way. 
Although, We keep working on adding new options everyday.

### [Elementor](https://wordpress.org/plugins/elementor/) page builder is required for this plugin.


== Installation ==

Note : This plugin works with Elementor. Make sure you have [Elementor](https://wordpress.org/plugins/elementor/) installed.

1. Download "Extender All In One For Elementor" plugin
2. Simply go to the Plugins page, then click on Add new and select the plugin's .zip file which is extender-all-in-one-for-elementor.zip".
3. Alternatively you can extract the contents of the zip file directly to your wp-content/plugins/ folder
4. Finally, activate the plugin.
5. Also you can see them under the category "Extender All In One For Elementor" on your element/widget list.


== Frequently Asked Questions ==

= Does it require to Elementor being Installed ? =

Yes.

= Is there any shortcode to use it in a page or post ? =

Not at the moment.

= Do I need Elementor PRO with this add-on?

No

= Why Should You Choose Extender All In One? =

Elementor editor gives you the flexibility to design beautiful sections. And Extender All In One Widget helps you to create beautiful content layout. This one widget can help you to build many different stylish layout and give you a space more show more content. 14 types of design you can make by all in one widget.

== Screenshots ==

1. Content Box Carousel
2. Content Box Grid
3. Icon Box Carousel
4. Icon Box Grid
5. Template Carousel
6. Template Grid
7. Image Carousel
8. Image Grid

Demo Link :[https://crazydevs.xyz/crazy-devs/widgets-demo/]

== Changelog ==

= 1.0.2 - 2022-07-14 =
- Tweak: Added Responsive Column option for Carousel.
- Fix: Changed elementor/widgets/register to elementor/widgets/register due to deprecated on Elementor version 3.5.0.
- Fix: Changed Elementor\Widgets_Manager::register_widget_type to Elementor\Widgets_Manager::register due to deprecated on Elementor version 3.5.0.

= 1.0.1 - 2021-12-26 =
- Fix: Bugs on Carousel and Slider due to the recent update of Elementor.

= 1.0 =
- Initial release


 == Upgrade Notice == 

Looking for upgrades? Please keep patience.
<?php 

// Elementor Saved Template 
function extender_template_options_widgets() {

    $templates = Elementor\Plugin::$instance->templates_manager->get_source( 'local' )->get_items();
    $types     = [];

    if ( empty($templates) ) {
        $template_options = ['0' => __('Template Not Found!', 'cam')];
    } else {
        $template_options = ['0' => __('Select Template', 'cam')];

        foreach ( $templates as $template ) {
            $template_options[$template['template_id']] = $template['title'] . ' (' . $template['type'] . ')';
            $types[$template['template_id']]            = $template['type'];
        }
    }

    return $template_options;
}
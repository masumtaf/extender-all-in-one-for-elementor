<?php
/**
 * Plugin Name: Extender All In One For Elementor
 * Plugin URI: https://gitlab.com/masumtaf/extender-all-in-one-for-elementor/
 * Description: The All in One Widgets you'll ever need.
 * Version: 1.0.2
 * Author: Abdullah
 * Author URI: https://gitlab.com/masumtaf
 * Text Domain: extender-all-in-one-for-elementor
 * Domain Path: /languages
 * License: GPL3
 */

namespace ExtenderAllInOne;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Extender_All_In_One_For_Elementor' ) ) {
    /**
     * Extender All In One For Elementor core class.
     *
     * Register plugin and make instances of core classes
     *
     * @package Extender-all-in-one-for-elementor
     * @version 1.0.0
     * @since   1.0.0
     */
    class Extender_All_In_One_For_Elementor {

        /**
         * Holds class instance
         *
         * @access private
         *
         * @var ExtenderAllInOne
         */
        private static $instance;

        /**
         * Appsero 
         * 
         */
        public $appsero = null;

    	/**
         * Default constructor.
         *
         * Initialize plugin core and build environment
         *
         * @since   1.0.0
         */
        public function __construct() {
            $this->define_constants();
            $this->add_actions();

        }

        /**
         * Get class instance
         *
         * @return ExtenderAllInOne
         */
        public static function get_instance(){
            if( null === self::$instance ){
                self::$instance = new self();
            }
            return self::$instance;
        }

        /**
         * Definition wrapper.
         *
         * Creates some useful def's in environment to handle
         * plugin paths
         *
         * @since   1.0.0
         */
        public function define_constants() {

            // Some Constants for ease of use
            if ( ! defined( 'EXTEND_VER' ) )
    			define( 'EXTEND_VER', '1.0.0' );
    		if ( ! defined( 'EXTEND_PNAME' ) )
    			define( 'EXTEND_PNAME', basename( dirname( __FILE__ ) ) );
    		if ( ! defined( 'EXTEND_PBNAME' ) )
    		define( 'EXTEND_PBNAME', plugin_basename(__FILE__) );
    		if ( ! defined( 'EXTEND_PATH' ) )
    			define( 'EXTEND_PATH', plugin_dir_path( __FILE__ ) );
            if ( ! defined( 'EXTEND_ELEMENTS' ) )
                define( 'EXTEND_ELEMENTS', plugin_dir_path( __FILE__ ) . 'elements/' );    
            if ( ! defined( 'EXTEND_INCLUDES' ) )
                define( 'EXTEND_INCLUDES', plugin_dir_path( __FILE__ ) . 'includes/' );
    		if ( ! defined( 'EXTEND_URL' ) )
    		define( 'EXTEND_URL', plugins_url( '/', __FILE__ ) );
    		if ( ! defined( 'EXTEND_ASSETS_URL' ) )
    			define( 'EXTEND_ASSETS_URL', EXTEND_URL . 'assets/' );
        }

        /**
         * Adds action hooks.
         *
         * @since   1.0.0
         */
        private function add_actions() {
            // Enqueue Styles and Scripts
            add_action( 'wp_enqueue_scripts', array( $this, 'extend_enqueue_scripts' ) );
            // Registering Elementor Widget Category
            add_action( 'elementor/elements/categories_registered', array( $this, 'extend_register_category' ) );
        	// Registering custom widgets
            add_action( 'elementor/widgets/register', array( $this, 'extend_add_elements' ) );
         
            // Plugin Loaded Action
            add_action( 'plugins_loaded', array( $this, 'extend_element_pack_load_plugin' ) );
            // Add Body Class 
            add_filter( 'body_class', array( $this, 'extend_add_body_classes' ) );
           
            require EXTEND_INCLUDES . 'helpers.php';
            require EXTEND_INCLUDES . 'traits/swiper-control.php';

        }

        /*
        *
        * Add Body Class extender-all-in-one-for-elementor
        */
        public function extend_add_body_classes( $classes ) {
            $classes[] = 'extender-addon extender-all-in-one-for-elementor';

            return $classes;
        }
        
        /**
         * Plugin load here correctly
         * Also loaded the language file from here
         */
        public function extend_element_pack_load_plugin() {
            load_plugin_textdomain( 'extender-all-in-one-for-elementor', false, basename( dirname( __FILE__ ) ) . '/languages' );

            if ( ! did_action( 'elementor/loaded' ) ) {
                add_action( 'admin_notices', array( $this, 'extend_element_pack_fail_load' ) );
                return;
            }
            
        }

        /**
        * Enqueue Plugin Styles and Scripts
        *
        */
        public function extend_enqueue_scripts() {
            wp_enqueue_style( 'extend-main-style', EXTEND_URL . 'assets/css/extend-style.min.css' );

            wp_register_script( 'swiper', EXTEND_URL . 'assets/vendor/js/swiper.min.js', array( 'jquery' ),  '1.0.1', true );
            wp_enqueue_script( 'extend-main-script', EXTEND_URL . 'assets/js/extend-script.min.js', array( 'jquery' ), false, true );
        }

        /**
        * Register Extender Elementor Addons category
        *
        */
        public function extend_register_category( $elements_manager ) {

            $elements_manager->add_category(
                'extender-addons',
                [
                    'title' => __( 'Extender All In One For Elementor', 'extender-all-in-one-for-elementor' ),
                    'icon' => 'fa fa-plug',
                ]
            );

        }

        /**
         * Check Elementor installed and activated correctly
         */
        function extend_element_pack_fail_load() {

            $message = sprintf(
                __( '%1$s requires %2$s to be installed and activated to function properly. %3$s', 'extender-all-in-one-for-elementor' ),
                '<strong>' . __( 'Extender All In One For Elementor', 'extender-all-in-one-for-elementor' ) . '</strong>',
                '<strong>' . __( 'Elementor', 'extender-all-in-one-for-elementor' ) . '</strong>',
                '<a href="' . esc_url( admin_url( 'plugin-install.php?s=Elementor&tab=search&type=term' ) ) . '">' . __( 'Please click here to install/activate Elementor', 'extender-all-in-one-for-elementor' ) . '</a>'
            );
        
            printf( '<div class="notice notice-warning is-dismissible"><p style="padding: 5px 0">%1$s</p></div>', $message );
        
        }

        /**
        * Include Addons
        *
        */
        public function extend_add_elements() {

        	include EXTEND_ELEMENTS . 'all-in-one/all-in-one.php';
            
        }

        /**
        * Check the elementor installed or not
        */
        public function _is_elementor_installed() {
            $file_path = 'elementor/elementor.php';
            $installed_plugins = get_plugins();

            return isset( $installed_plugins[ $file_path ] );
        }


    }

// Instance of the Extender All In One class
\ExtenderAllInOne\Extender_All_In_One_For_Elementor::get_instance();

}
<?php
namespace ExtenderAllInOne\Elements;

use Elementor\Widget_Base;
use \Elementor\Plugin;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Css_Filter;
use Elementor\Icons_Manager;
use Elementor\Utils;
use Elementor\Repeater;
use Elementor\Control_Media;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Group_Control_Image_Size;
use ExtenderAllInOne\Includes\Traits\Extender_Swiper_Controls;


if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly 
}

class All_In_One extends Widget_Base {

    use Extender_Swiper_Controls;

	public function get_name() {
		return 'extend-all-in-one';
	}

	public function get_title() {
		return __( 'All In One', 'extender-all-in-one-for-elementor' );
	}

	public function get_icon() {
		return 'eicon-media-carousel';
	}

	public function get_categories() {
		return [ 'extender-addons' ];
	}

	public function get_keywords() {
		return [ 'image', 'custom carousel', 'slider', 'service', 'box carousel', 'info carousel', 'logo carousel', 'all in one' ];
	}

	public function get_script_depends() {
      	return [ 'swiper' ];
    }

	protected function register_controls() {
		$extend_primary_color = '#fd71af';

		$this->start_controls_section(
			'extend_all_in_one_content',
			array(
				'label' => __( 'Content Layout', 'extender-all-in-one-for-elementor' ),
				'type'  => Controls_Manager::TAB_CONTENT,
			)
		);

		$this->add_control(
			'extend_content_layout_type',
			[
				'label'   => __( 'Layout Type', 'extender-all-in-one-for-elementor' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'carousel',
				'seperator' => 'after',
				'options' => [
					'carousel'      => __( 'Carousel', 'extender-all-in-one-for-elementor' ),
					'grid'     		=> __( 'Grid', 'extender-all-in-one-for-elementor' ),
				]
			]
		);

		$this->add_responsive_control(
            'extend_columns',
            [
                'label'              => esc_html__('Columns', 'extender-all-in-one-for-elementor'),
                'type'               => Controls_Manager::SELECT,
                'default'            => '3',
                'tablet_default'     => '2',
                'mobile_default'     => '1',
                'options'            => [
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                ],
                'frontend_available' => true,
				'condition' => [
					'extend_content_layout_type' => [ 'grid', 'mesonry' ],
				]
            ]
        );

		$this->add_responsive_control(
            'extend_content_grid_container_margin',
            [
                'label'         => esc_html__( 'Margin', 'extender-all-in-one-for-elementor' ),
                'type'          => Controls_Manager::DIMENSIONS,
                'default'       => [
                    'top'       => '0',
                    'right'     => '0',
                    'bottom'    => '20',
                    'left'      => '0',
                    'isLinked'  => false
                ],                
                'size_units'    => [ 'px', 'em', '%' ],
                'selectors'     => [
                        '{{WRAPPER}} .extend-content-grid-item .extend-content-item-container' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
		);
		
		$this->add_control(
			'extend_content_grid_container_padding',
			[
				'label'      => esc_html__( 'Padding', 'extender-all-in-one-for-elementor' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px' ],
				'selectors'  => [
					'{{WRAPPER}} .extend-content-grid-item .extend-content-item-container'=> 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				]
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'extend_custom_content_repeater',
			array(
				'label' => __( 'Content Repeater', 'extender-all-in-one-for-elementor' ),
				'type'  => Controls_Manager::TAB_CONTENT,
			)
		);

		$extend_repeater = new Repeater();

		$extend_repeater->add_control(
			'extend_all_in_one_content_type',
			[
				'label'   => __( 'Content Type', 'extender-all-in-one-for-elementor' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'content',
				'seperator' => 'after',
				'options' => [
					'content'       => __( 'Content', 'extender-all-in-one-for-elementor' ),
					'shortcode'     => __( 'ShortCode', 'extender-all-in-one-for-elementor' ),
					'image'     	=> __( 'Image', 'extender-all-in-one-for-elementor' ),
					'save_template' => __( 'Save Template', 'extender-all-in-one-for-elementor' ),
				]
			]
		);

		// Content As Template
		$extend_repeater->add_control(
			'extend_all_in_one_save_template',
			[
				'label'     => __( 'Select Section', 'extender-all-in-one-for-elementor' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => extender_template_options_widgets(),
				'default'   => '-1',
				'condition' => [
					'extend_all_in_one_content_type' => 'save_template'
				]
			]
		);

		// Content As Shortcode
		$extend_repeater->add_control(
			'extend_all_in_one_shortcode',
			[
				'label'       => __( 'Enter your shortcode', 'extender-all-in-one-for-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'label_block' => true,
				'placeholder' => __( '[shortcode]', 'extender-all-in-one-for-elementor' ),
				'condition'   => [
					'extend_all_in_one_content_type' => 'shortcode'
				]
			]
		);

		// Content As Image
		$extend_repeater->add_control(
			'extend_all_in_one_image',
			[
				'label' => esc_html__( 'Choose Image', 'extender-all-in-one-for-elementor' ),
				'type'  => Controls_Manager::MEDIA,
				'default'     => [
                    'url'     => Utils::get_placeholder_image_src()
                ],
				'dynamic' => [ 'active' => true ],
				'condition' => [
					'extend_all_in_one_content_type' => ['image']
				]
			]
		);

		$extend_repeater->add_group_control(
            Group_Control_Image_Size::get_type(),
            [
				'name'    => 'extend_all_in_one_image_size',
				'label'   => esc_html__( 'Image Type', 'extender-all-in-one-for-elementor' ),
				'default' => 'full',
				'condition' => [
					'extend_all_in_one_content_type' => ['image']
				]
            ]
		);

		$extend_repeater->add_control(
			'extend_all_in_one_image_link_to_type',
			[
				'label'   => esc_html__( 'Link to', 'extender-all-in-one-for-elementor' ),
				'type'    => Controls_Manager::SELECT,
				'options' => [
					''       => esc_html__( 'None', 'extender-all-in-one-for-elementor' ),
					'file'   => esc_html__( 'Media File', 'extender-all-in-one-for-elementor' ),
					'custom' => esc_html__( 'Custom URL', 'extender-all-in-one-for-elementor' ),
				],
				'condition' => [
					'extend_all_in_one_content_type' => ['image']
				]
			]
		);

		$extend_repeater->add_control(
			'extend_all_in_one_image_link_to',
			[
				'type'        => Controls_Manager::URL,
				'placeholder' => 'http://your-link.com',
				'dynamic'     => [ 'active' => true ],
				'condition' => [
					'extend_all_in_one_content_type' => ['image'],
					'extend_all_in_one_image_link_to_type' => 'custom',
				],
				'separator'  => 'none',
				'show_label' => false,
			]
		);

		// Content box

		$extend_repeater->start_controls_tabs( 'extend_all_in_one_content_tabs' );

		$extend_repeater->start_controls_tab(
			'extend_all_in_one_content_tabs_tabs_item_normal',
			[
				'label' => __( 'Heading', 'extender-all-in-one-for-elementor' ),
				'condition' => [
					'extend_all_in_one_content_type' => 'content'
				]
			]
		);

		$extend_repeater->add_control(
			'extend_all_in_one_content_box_type',
			[
				'label'        => esc_html__('Content Type', 'extender-all-in-one-for-elementor'),
				'type'         => Controls_Manager::CHOOSE,
				'toggle'       => false,
				'default'      => 'extend_content_image',
				'prefix_class' => 'extend-icon-type-',
				'render_type'  => 'template',
				'options'      => [
					'extend_content_icon' => [
						'title' => esc_html__('Icon', 'extender-all-in-one-for-elementor'),
						'icon'  => 'fas fa-star'
					],
					'extend_content_image' => [
						'title' => esc_html__('Image', 'extender-all-in-one-for-elementor'),
						'icon'  => 'far fa-image'
					]
				],
				'condition' => [
					'extend_all_in_one_content_type' => 'content'
				]
			]
		);

		$extend_repeater->add_control(
			'extend_all_in_one_content_selected_icon',
			[
				'label'            => __( 'Icon', 'extender-all-in-one-for-elementor' ),
				'type'             => Controls_Manager::ICONS,
				'fa4compatibility' => 'icon',
				'default' => [
					'value' => 'fas fa-star',
					'library' => 'fa-solid',
				],
				'render_type'      => 'template',
				'condition'        => [
					'extend_all_in_one_content_box_type' => 'extend_content_icon',
				],
				'label_block' => false,
				'skin' => 'inline'
			]
		);

		$extend_repeater->add_control(
			'extend_all_in_one_content_selected_image',
			[
				'label'       => __( 'Image Icon', 'extender-all-in-one-for-elementor' ),
				'type'        => Controls_Manager::MEDIA,
				'render_type' => 'template',
				'default'     => [
					'url' => Utils::get_placeholder_image_src(),
				],
				'condition' => [
					'extend_all_in_one_content_box_type' => 'extend_content_image',
					'extend_all_in_one_content_type' => 'content'
				]
			]
		);

		$extend_repeater->add_group_control(
            Group_Control_Image_Size::get_type(),
            [
				'name'    => 'extend_all_in_one_content_selected_image_size',
				'label'   => esc_html__( 'Image Size', 'extender-all-in-one-for-elementor' ),
				'default' => 'full',
				'condition' => [
					'extend_all_in_one_content_type' => 'content',
					'extend_all_in_one_content_box_type' => 'extend_content_image',
					'extend_all_in_one_content_selected_image!' => '',
				]
            ]
		);

		$extend_repeater->add_control(
			'extend_all_in_one_content_icon_image_link_to_yes',
			[
				'label'        => __( 'Icon & Iamge Link ?', 'extender-all-in-one-for-elementor' ),
				'type'         => Controls_Manager::SWITCHER,
				'default'      => 'no',
				'return_value' => 'yes',
				'condition' => [
					'extend_all_in_one_content_type' => 'content'
				]
			]
		);

		$extend_repeater->add_control(
			'extend_all_in_one_content_icon_image_link_to',
			[
				'type'        => Controls_Manager::URL,
				'placeholder' => 'http://your-link.com',
				'dynamic'     => [ 'active' => true ],
				'condition' => [
					'extend_all_in_one_content_type' => ['content'],
					'extend_all_in_one_content_box_type' => ['extend_content_image', 'extend_content_icon'],
					'extend_all_in_one_content_icon_image_link_to_yes' => 'yes',
				],
				'separator'  => 'after',
				'show_label' => false,
			]
		);

		$extend_repeater->add_control(
			'extend_all_in_one_content_show_before_heading',
			[
				'label'        => __( 'Show Before Heading', 'extender-all-in-one-for-elementor' ),
				'type'         => Controls_Manager::SWITCHER,
				'default'      => 'yes',
				'return_value' => 'yes',
				'condition' => [
					'extend_all_in_one_content_type' => 'content'
				]
			]
		);

		$extend_repeater->add_control(
			'extend_all_in_one_content_before_heading', 
			[
				'label'       => __( 'Before Heading', 'extender-all-in-one-for-elementor' ),
				'type'        => Controls_Manager::TEXT,
				'dynamic'     => [ 'active' => true ],
				'label_block' => true,
				'default'     => __( 'Before Heading' , 'extender-all-in-one-for-elementor' ),
				'condition' => [
					'extend_all_in_one_content_type' => 'content',
					'extend_all_in_one_content_show_before_heading' => 'yes'
				]
			]
		);

		$extend_repeater->add_control(
			'extend_all_in_one_content_show_heading',
			[
				'label'        => __( 'Show Heading', 'extender-all-in-one-for-elementor' ),
				'type'         => Controls_Manager::SWITCHER,
				'default'      => 'yes',
				'return_value' => 'yes',
				'condition' => [
					'extend_all_in_one_content_type' => 'content'
				]
			]
		);

		$extend_repeater->add_control(
			'extend_all_in_one_content_heading', 
			[
				'label'       => __( 'Heading', 'extender-all-in-one-for-elementor' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => __( 'Heading' , 'extender-all-in-one-for-elementor' ),
				'dynamic'     => [ 'active' => true ],
				'label_block' => true,
				'condition' => [
					'extend_all_in_one_content_type' => 'content',
					'extend_all_in_one_content_show_heading' => 'yes'
				]
				
			]
		);

		$extend_repeater->add_control(
			'extend_all_in_one_content_heading_link', 
			[
				'label'         => esc_html__( 'Heading Link', 'extender-all-in-one-for-elementor' ),
				'type'          => Controls_Manager::URL,
				'show_external' => false,
				'dynamic'       => [ 'active' => true ],
				'condition' => [
					'extend_all_in_one_content_type' => 'content',
					'extend_all_in_one_content_show_heading' => 'yes',
					'extend_all_in_one_content_heading!' => ''
				]
			]
		);


		$extend_repeater->add_control(
			'extend_all_in_one_content_show_after_heading',
			[
				'label'        => __( 'Show After Heading', 'extender-all-in-one-for-elementor' ),
				'type'         => Controls_Manager::SWITCHER,
				'default'      => 'yes',
				'return_value' => 'yes',
				'condition' => [
					'extend_all_in_one_content_type' => 'content'
				]
			]
		);

		$extend_repeater->add_control(
			'extend_all_in_one_content_after_heading', 
			[
				'label'       => __( 'After Heading', 'extender-all-in-one-for-elementor' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => __( 'After Heading' , 'extender-all-in-one-for-elementor' ),
				'dynamic'     => [ 'active' => true ],
				'label_block' => true,
				'condition' => [
					'extend_all_in_one_content_type' => 'content',
					'extend_all_in_one_content_show_after_heading' => 'yes'
				]
				
			]
		);

		$extend_repeater->end_controls_tab();

		$extend_repeater->start_controls_tab(
			'extend_all_in_one_content_tabs_tabs_item_content',
			[
				'label' => __( 'Content', 'extender-all-in-one-for-elementor' ),
				'condition' => [
					'extend_all_in_one_content_type' => 'content'
				]
			]
		);

		$extend_repeater->add_control(
			'extend_all_in_one_content_show_content',
			[
				'label'        => __( 'Show Content', 'extender-all-in-one-for-elementor' ),
				'type'         => Controls_Manager::SWITCHER,
				'default'      => 'yes',
				'return_value' => 'yes',
				'condition' => [
					'extend_all_in_one_content_type' => 'content'
				]
			]
		);

		$extend_repeater->add_control(
			'extend_all_in_one_content',
			[
				'label'   => esc_html__( 'C O N T E N T', 'extender-all-in-one-for-elementor' ),
				'type'    => Controls_Manager::WYSIWYG,
				'default' => esc_html__( 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'extender-all-in-one-for-elementor' ),
				'seperator' => 'before',
				'condition' => [
					'extend_all_in_one_content_type' => 'content',
					'extend_all_in_one_content_show_content' => 'yes'
				]
			]
		);

		$extend_repeater->add_control(
			'extend_all_in_one_content_show_box_button',
			[
				'label'        => __( 'Show Button', 'extender-all-in-one-for-elementor' ),
				'type'         => Controls_Manager::SWITCHER,
				'default'      => 'yes',
				'return_value' => 'yes',
				'condition' => [
					'extend_all_in_one_content_type' => 'content'
				]
			]
		);

		$extend_repeater->add_control(
			'extend_all_in_one_content_show_box_button_icon',
			[
				'label'       => __( 'Icon', 'extender-all-in-one-for-elementor' ),
				'type'             => Controls_Manager::ICONS,
				'fa4compatibility' => 'readmore_icon',
				'condition'   => [
					'extend_all_in_one_content_show_box_button'       => 'yes'
				],
				'label_block' => false,
				'skin' => 'inline'
			]
		);

		$extend_repeater->add_control(
			'extend_all_in_one_content_show_box_button_icon_align',
			[
				'label'   => __( 'Icon Position', 'extender-all-in-one-for-elementor' ),
				'type'    => Controls_Manager::CHOOSE,
				'default' => 'right',
				'options' => [
					'left'   => [
						'title' => __( 'Left', 'extender-all-in-one-for-elementor' ),
						'icon'  => 'eicon-arrow-left',
						],
					'right'  => [
						'title' => __( 'Right', 'extender-all-in-one-for-elementor' ),
						'icon'  => 'eicon-arrow-right',
						],
				],
				'condition' => [
					'extend_all_in_one_content_show_box_button_icon[value]!' => '',
				],
			]
		);
		
		$extend_repeater->add_control(
			'extend_all_in_one_content_box_button', 
			[
				'label'       => esc_html__( 'Button Text', 'extender-all-in-one-for-elementor' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'Read More' , 'extender-all-in-one-for-elementor' ),
				'label_block' => true,
				'dynamic'     => [ 'active' => true ],
				'condition' => [
					'extend_all_in_one_content_type' => 'content',
					'extend_all_in_one_content_show_box_button' => 'yes'
				]
			]
		);

		$extend_repeater->add_control(
			'extend_all_in_one_content_button_link', 
			[
				'label'         => esc_html__( 'Button Link', 'extender-all-in-one-for-elementor' ),
				'type'          => Controls_Manager::URL,
				'show_external' => false,
				'dynamic'       => [ 'active' => true ],
				'condition'     => [
					'extend_all_in_one_content_type' => 'content',
					'extend_all_in_one_content_show_box_button' => 'yes',
					'extend_all_in_one_content_box_button!' => ''
				]
			]
		);

		$extend_repeater->add_control(
			'extend_all_in_one_content_box_button_animation',
			[
				'label' => __( 'Hover Animation', 'extender-all-in-one-for-elementor' ),
				'type' => Controls_Manager::HOVER_ANIMATION,
			]
		);

		
		$extend_repeater->end_controls_tab();

		$extend_repeater->end_controls_tabs();

		// Content As Repeater Shows
		$this->add_control(
			'extend_all_in_one_repeater',
			[
				'label'   => esc_html__( 'All In one Reapeter', 'extender-all-in-one-for-elementor' ),
				'type'      => Controls_Manager::REPEATER,
				'fields'  => $extend_repeater->get_controls(),
				'default' => [
					[
						'extend_all_in_one_content_heading'   => esc_html__( 'Content Box 1', 'extender-all-in-one-for-elementor' ),
						'extend_all_in_one_content_selected_image' => ['url' => Utils::get_placeholder_image_src()],

					],
					[
						'extend_all_in_one_content_heading'   => esc_html__( 'Content Box 1', 'extender-all-in-one-for-elementor' ),
						'extend_all_in_one_content_selected_image' => ['url' => Utils::get_placeholder_image_src()],

					],
					[
						'extend_all_in_one_content_heading'   => esc_html__( 'Content Box 1', 'extender-all-in-one-for-elementor' ),
						'extend_all_in_one_content_selected_image' => ['url' => Utils::get_placeholder_image_src()],

					],
				],
				'seperator' => 'before',

			]
		);

        $this->end_controls_section();




		// Carousel Settings
        $this->extender_register_carousel_controls();

		// Carousel Navigation Settings
        $this->extender_register_navigation_controls();

		/*
		* Content Carousel container Styling Section
		*/
		$this->start_controls_section(
			'extend_all_in_one_content_item_styles',
			[
				'label' => esc_html__( 'Container', 'extender-all-in-one-for-elementor' ),
				'tab'   => Controls_Manager::TAB_STYLE
			]
		);

		$this->add_control(
			'extend_all_in_one_content_item_layout',
			[
				'label' => __( 'Layout', 'extender-all-in-one-for-elementor' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'layout-1',
				'options' => [
					'layout-1'  => __( 'Layout 1', 'extender-all-in-one-for-elementor' ),
					'layout-2' => __( 'Layout 2', 'extender-all-in-one-for-elementor' ),
				],
			]
		);

		$this->add_control(
			'extend_all_in_one_content_item_container_alignment',
			[
				'label'   => __( 'Alignment', 'extender-all-in-one-for-elementor' ),
				'type'    => Controls_Manager::CHOOSE,
				'toggle'  => false,
				'default' => 'eleft',
				'options' => [
					'left'   => [
						'title' => __( 'Left', 'extender-all-in-one-for-elementor' ),
						'icon'  => 'eicon-text-align-left'
					],
					'center' => [
						'title' => __( 'middle', 'extender-all-in-one-for-elementor' ),
						'icon'  => 'eicon-text-align-center'
					],
					'right'  => [
						'title' => __( 'Right', 'extender-all-in-one-for-elementor' ),
						'icon'  => 'eicon-text-align-right'
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .extend-content-grid-item .extend-content-item-container' => 'text-align: {{VALUE}};'
				]
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name'     => 'extend_all_in_one_content_item_container_background',
				'types'    => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .extend-content-grid-item .extend-content-item-container'
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'            => 'extend_all_in_one_content_item_container_border',
				'fields_options'  => [
                    'border'      => [
                        'default' => 'solid'
                    ],
                    'width'          => [
                        'default'    => [
							'top'    => '1',
							'right'  => '1',
							'bottom' => '1',
							'left'   => '1'
                        ]
                    ],
                    'color'       => [
                        'default' => '#e3e3e3'
                    ]
				],
				'selector'        => '{{WRAPPER}} .extend-content-grid-item .extend-content-item-container'
			]
		);

		$this->add_responsive_control(
			'extend_all_in_one_content_item_container_radius',
			[
				'label'      => __( 'Border radius', 'extender-all-in-one-for-elementor' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'separator'  => 'before',
				'default'    => [
					'top'    => '10',
					'right'  => '10',
					'bottom' => '10',
					'left'   => '10'
				],
				'selectors'  => [
					'{{WRAPPER}} .extend-content-grid-item .extend-content-item-container' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				]
			]
		);

		$this->add_responsive_control(
			'extend_all_in_one_content_item_container_padding',
			[
				'label'      => __( 'Padding', 'extender-all-in-one-for-elementor' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'selectors'  => [
					'{{WRAPPER}} .extend-content-grid-item .extend-content-item-container' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
				]
			]
		);

		$this->add_responsive_control(
			'extend_all_in_one_content_item_container_margin',
			[
				'label'      => __( 'Margin', 'extender-all-in-one-for-elementor' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'selectors'  => [
					'{{WRAPPER}} .extend-content-grid-item .extend-content-item-container' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
				]
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name'     => 'extend_all_in_one_content_item_container_box_shadow',
				'selector' => '{{WRAPPER}} .extend-content-grid-item .extend-content-item-container'
			]
		);

		$this->end_controls_section();

		// icon $ image content box start 
		//Style
		$this->start_controls_section(
			'extend_content_box_section_icon_box',
			[
				'label'      => __( 'Icon & Image', 'extender-all-in-one-for-elementor' ),
				'tab'        => Controls_Manager::TAB_STYLE,
				'conditions' => [
				
				]
			]
		);

		$this->add_control(
			'extend_content_box_img_icon_position',
			[
				'label'     => __( 'Icon Position', 'extender-all-in-one-for-elementor' ),
				'type'      => Controls_Manager::CHOOSE,
				'separator' => 'before',
				'default'   => 'top',
				'options'   => [
					'left' => [
						'title' => __( 'Left', 'extender-all-in-one-for-elementor' ),
						'icon'  => 'eicon-h-align-left',
					],
					'top' => [
						'title' => __( 'Top', 'extender-all-in-one-for-elementor' ),
						'icon'  => 'eicon-v-align-top',
					],
					'right' => [
						'title' => __( 'Right', 'extender-all-in-one-for-elementor' ),
						'icon'  => 'eicon-h-align-right',
					],
				],
				'prefix_class' => 'elementor-position-',
				'toggle'       => false,
				'render_type' => 'template',
				'conditions' => [
			
				]
			]
		);

		$this->add_control(
			'extend_content_box_img_icon_icon_vertical_alignment',
			[
				'label'   => __( 'Icon Vertical Alignment', 'extender-all-in-one-for-elementor' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'top'   => [
						'title' => __( 'Top', 'extender-all-in-one-for-elementor' ),
						'icon'  => 'eicon-v-align-top',
					],
					'middle' => [
						'title' => __( 'Middle', 'extender-all-in-one-for-elementor' ),
						'icon'  => 'eicon-v-align-middle',
					],
					'bottom' => [
						'title' => __( 'Bottom', 'extender-all-in-one-for-elementor' ),
						'icon'  => 'eicon-v-align-bottom',
					],
				],
				'selectors_dictionary' => [
					'top' => 'align-items: baseline;',
					'middle' => 'align-items: center;',
					'bottom' => 'align-items: end;',
				],
				'selectors'      => [
                    '{{WRAPPER}} .extend-content-item-container.container-grid.layout-2 .extand-content-type-content-box' => '{{VALUE}};',
                    '{{WRAPPER}} .extend-content-item-container.container-carousel.layout-2 .extand-content-type-content-box' => '{{VALUE}};',
                ],
				'default'      => 'top',
				'toggle'       => false,
				'prefix_class' => 'elementor-vertical-align-',
				'condition'    => [
					'extend_content_box_img_icon_position' => ['left', 'right'],
				],
			]
		);

		$this->start_controls_tabs( 'extend_content_box_icon_colors' );

		$this->start_controls_tab(
			'extend_content_box_icon_colors_normal',
			[
				'label' => __( 'Normal', 'extender-all-in-one-for-elementor' ),
			]
		);

		$this->add_control(
			'extend_content_box_icon_color',
			[
				'label'     => __( 'Icon Color', 'extender-all-in-one-for-elementor' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .extand-content-type-content-box .extand-content-icon a' => 'color: {{VALUE}};',
					'{{WRAPPER}} .extand-content-type-content-box .extand-content-icon a i' => 'color: {{VALUE}};',
					'{{WRAPPER}} .extand-content-type-content-box .extand-content-icon' => 'color: {{VALUE}};',
				],
				'condition' => [
					
				],
			]
		);

		$this->add_control(
			'extend_content_box_show_svg_icon_color',
			[
				'label'     => __( 'Svg Icon Color', 'extender-all-in-one-for-elementor' ),
				'type'      => Controls_Manager::SWITCHER,
				'condition' => [
					
				],
			]
		);

		$this->add_control(
			'extend_content_box_svg_icon_fill_color',
			[
				'label'     => __( 'Fill Color', 'extender-all-in-one-for-elementor' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .extand-content-type-content-box .extand-content-icon svg *' => 'fill: {{VALUE}};',
				],
				'condition' => [
				
				],
			]
		);

		$this->add_control(
			'extend_content_box_svg_icon_stroke_color',
			[
				'label'     => __( 'Stroke Color', 'extender-all-in-one-for-elementor' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .extand-content-type-content-box .extand-content-icon svg *' => 'stroke: {{VALUE}};',
				],
				'condition' => [
					
				],
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name'      => 'extend_content_box_icon_background',
				'selector'  => '{{WRAPPER}} .extand-content-type-content-box .extand-content-icon, {{WRAPPER}} .extand-content-type-content-box .extand-content-image',
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'        => 'extend_content_box_icon_border',
				'placeholder' => '1px',
				'default'     => '1px',
				'selector'    => '{{WRAPPER}} .extand-content-type-content-box .extand-content-icon, {{WRAPPER}} .extand-content-type-content-box .extand-content-image'
			]
		);

		$this->add_responsive_control(
			'extend_content_box_icon_radius',
			[
				'label'      => esc_html__('Border Radius', 'extender-all-in-one-for-elementor'),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .extand-content-type-content-box .extand-content-icon' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}; overflow: hidden;',
					'{{WRAPPER}} .extand-content-type-content-box .extand-content-image' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}; overflow: hidden;',
				],
				'condition' => [
					
				],
			]
		);

		$this->add_control(
			'extend_content_box_icon_radius_advanced_show',
			[
				'label' => __( 'Advanced Radius', 'extender-all-in-one-for-elementor' ),
				'type'  => Controls_Manager::SWITCHER,
			]
		);	

		$this->add_control(
			'extend_content_box_icon_radius_advanced_animation',
			[
				'label' => __( 'Animation', 'extender-all-in-one-for-elementor' ),
				'type'  => Controls_Manager::SWITCHER,
				'prefix_class' => 'extand-icon-effect-',
			]
		);

		$this->add_control(
			'extend_content_box_icon_radius_advanced',
			[
				'label'       => esc_html__('Radius', 'extender-all-in-one-for-elementor'),
				'description' => sprintf(__('For example: <b>%1s</b> or Go <a href="%2s" target="_blank">this link</a> and copy and paste the radius value.', 'extender-all-in-one-for-elementor'), '75% 25% 43% 57% / 46% 29% 71% 54%', 'https://9elements.github.io/fancy-border-radius/'),
				'type'        => Controls_Manager::TEXT,
				'size_units'  => [ 'px', '%' ],
				'default'     => '75% 25% 43% 57% / 46% 29% 71% 54%',
				'selectors'   => [
					'{{WRAPPER}} .extand-content-type-content-box .extand-content-icon'     => 'border-radius: {{VALUE}}; overflow: hidden;',
					'{{WRAPPER}} .extand-content-type-content-box .extand-content-image' => 'border-radius: {{VALUE}}; overflow: hidden;',
					'{{WRAPPER}} .extand-content-type-content-box .extand-content-image img' => 'border-radius: {{VALUE}}; overflow: hidden;'
				],
				'condition' => [
					'extend_content_box_icon_radius_advanced_show' => 'yes',
				],
			]
		);

		$this->add_responsive_control(
			'extend_content_box_icon_padding',
			[
				'label'      => esc_html__('Padding', 'extender-all-in-one-for-elementor'),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .extand-content-type-content-box .extand-content-icon' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					'{{WRAPPER}} .extand-content-type-content-box .extand-content-image' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
				]
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name'     => 'extend_content_box_icon_shadow',
				'selector' => '{{WRAPPER}} .extand-content-type-content-box .extand-content-icon, {{WRAPPER}} .extand-content-type-content-box .extand-content-image'
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'      => 'extend_content_box_icon_typography',
				'selector'  => '{{WRAPPER}} .extand-content-type-content-box .extand-content-icon, {{WRAPPER}} .extand-content-type-content-box .extand-content-image',
				'condition' => [
				
				],
			]
		);

		$this->add_responsive_control(
			'extend_content_box_icon_space',
			[
				'label'     => __( 'Spacing', 'extender-all-in-one-for-elementor' ),
				'type'      => Controls_Manager::SLIDER,
				'separator' => 'before',
				'default'   => [
					'size' => 15,
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}}.elementor-position-right .extand-content-icon' => 'margin-left: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}}.elementor-position-left .extand-content-icon'  => 'margin-right: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}}.elementor-position-top .extand-content-icon'   => 'margin-bottom: {{SIZE}}{{UNIT}};',
					'(mobile){{WRAPPER}} .extand-content-icon'                  => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'extend_content_box_icon_fullwidth',
			[
				'label' => __( 'Image Fullwidth', 'extender-all-in-one-for-elementor' ),
				'type'  => Controls_Manager::SWITCHER,
				'selectors' => [
					'{{WRAPPER}} .extand-content-type-content-box .extand-content-image img' => 'width: 100%;box-sizing: border-box;',
				],
				'condition' => [
					'extend_content_box_icon_fullwidth' => 'extend_content_box_icon_fullwidth'
				]
			]
		);

		$this->add_responsive_control(
			'extend_content_box_icon_size',
			[
				'label' => __( 'Size', 'extender-all-in-one-for-elementor' ),
				'type'  => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 6,
						'max' => 300,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .extand-content-type-content-box .extand-content-icon' => 'font-size: {{SIZE}}{{UNIT}}; width: {{SIZE}}{{UNIT}};',
				],
				'conditions' => [
					
				]
			]
		);

		$this->add_control(
			'extend_content_box_rotate',
			[
				'label'   => __( 'Rotate', 'extender-all-in-one-for-elementor' ),
				'type'    => Controls_Manager::SLIDER,
				'default' => [
					'size' => 0,
					'unit' => 'deg',
				],
				'range' => [
					'deg' => [
						'max'  => 360,
						'min'  => -360,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .extand-content-type-content-box .extand-content-icon i, {{WRAPPER}} .extand-content-type-content-box .extand-content-image img, {{WRAPPER}} .extand-content-type-content-box .extand-content-icon svg'   => 'transform: rotate({{SIZE}}{{UNIT}});',
				],
			]
		);

		$this->add_control(
			'extend_content_box_image_opacity',
			[
				'label' => __( 'Opacity', 'extender-all-in-one-for-elementor' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'max' => 1,
						'min' => 0.10,
						'step' => 0.01,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .extand-content-type-content-box .extand-content-image img' => 'opacity: {{SIZE}};',
				],
				'condition' => [
					
				],
			]
		);

		$this->add_control(
			'extend_content_box_background_hover_transition',
			[
				'label' => __( 'Transition Duration', 'extender-all-in-one-for-elementor' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 0.3,
				],
				'range' => [
					'px' => [
						'max' => 3,
						'step' => 0.1,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .extand-content-type-content-box .extand-content-image *' => 'transition-duration: {{SIZE}}s',
					'{{WRAPPER}} .extand-content-type-content-box .extand-content-icon *' => 'transition-duration: {{SIZE}}s',
				],
				'condition' => [
				
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'extend_content_box_icon_hover',
			[
				'label' => __( 'Hover', 'extender-all-in-one-for-elementor' ),
			]
		);

		$this->add_control(
			'extend_content_box_icon_hover_color',
			[
				'label'     => __( 'Icon Color', 'extender-all-in-one-for-elementor' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .extand-content-type-content-box:hover .extand-content-image' => 'color: {{VALUE}};',
					'{{WRAPPER}} .extand-content-type-content-box:hover .extand-content-icon' => 'color: {{VALUE}};',
					'{{WRAPPER}} .extand-content-type-content-box:hover .extand-content-icon a' => 'color: {{VALUE}};',
					'{{WRAPPER}} .extand-content-type-content-box:hover .extand-content-icon a i' => 'color: {{VALUE}};',
				],
				'condition' => [
				
				],
			]
		);

		$this->add_control(
			'extend_content_box_svg_icon_hover_fill_color',
			[
				'label'     => __( 'Fill Color', 'extender-all-in-one-for-elementor' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .extand-content-type-content-box:hover .extand-content-icon svg *' => 'fill: {{VALUE}};',
				],
				'condition' => [
				
				],
			]
		);

		$this->add_control(
			'extend_content_box_svg_icon_hover_stroke_color',
			[
				'label'     => __( 'Stroke Color', 'extender-all-in-one-for-elementor' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .extand-content-type-content-box:hover .extand-content-icon svg *' => 'stroke: {{VALUE}};',
				],
				'condition' => [
					
				],
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name'      => 'extend_content_box_icon_hover_background',
				'separator' => 'before',
				'selector'  => '{{WRAPPER}} .extand-content-type-content-box:hover .extand-content-icon',
			]
		);

		$this->add_control(
			'extend_content_box_icon_hover_border_color',
			[
				'label'     => __( 'Border Color', 'extender-all-in-one-for-elementor' ),
				'type'      => Controls_Manager::COLOR,
				'separator' => 'before',
				'selectors' => [
					'{{WRAPPER}} .{{WRAPPER}} .extand-content-type-content-box:hover .extand-content-icon'  => 'border-color: {{VALUE}};',
				],
				'condition' => [
					
				],
			]
		);

		$this->add_responsive_control(
			'extend_content_box_icon_hover_radius',
			[
				'label'      => esc_html__('Border Radius', 'extender-all-in-one-for-elementor'),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'separator'  => 'after',
				'selectors'  => [
					'{{WRAPPER}} .extand-content-type-content-box:hover .extand-content-image' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}; overflow: hidden;',
					'{{WRAPPER}} .extand-content-type-content-box:hover .extand-content-image img' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}; overflow: hidden;'
				]
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name'     => 'extend_content_box_icon_hover_shadow',
				'selector' => '{{WRAPPER}} .extand-content-type-content-box:hover .extand-content-image'
			]
		);

		$this->add_control(
			'extend_content_box_icon_hover_rotate',
			[
				'label'   => __( 'Rotate', 'extender-all-in-one-for-elementor' ),
				'type'    => Controls_Manager::SLIDER,
				'default' => [
					'unit' => 'deg',
				],
				'range' => [
					'deg' => [
						'max'  => 360,
						'min'  => -360,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .extand-content-type-content-box:hover .extand-content-image i, {{WRAPPER}} .extand-content-type-content-box:hover .extand-content-image img, {{WRAPPER}} .extand-content-type-content-box:hover .extand-content-image svg'   => 'transform: rotate({{SIZE}}{{UNIT}});',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Css_Filter::get_type(),
			[
				'name'      => 'extend_content_box_css_filters_hover',
				'selector'  => '{{WRAPPER}} .extand-content-type-content-box:hover .extand-content-image img',
				'condition' => [
					
				],
			]
		);

		$this->add_control(
			'extend_content_box_image_opacity_hover',
			[
				'label' => __( 'Opacity', 'extender-all-in-one-for-elementor' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'max' => 1,
						'min' => 0.10,
						'step' => 0.01,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .extand-content-type-content-box .extand-content-image:hover img' => 'opacity: {{SIZE}};',
				],
				'condition' => [
					
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
		// icon $ image content box End 

		$this->start_controls_section(
			'extend_all_in_one_content_heading',
			[
				'label' => __( 'Heading', 'extender-all-in-one-for-elementor' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		); 

		$this->add_responsive_control(
			'extend_all_in_one_content_before_heading_top_space',
			[
				'label' => __( 'Before heading top Spacing', 'extender-all-in-one-for-elementor' ),
				'type'  => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .extend-content-item-container .extand-content-box-before-heading' => 'margin-top: {{SIZE}}{{UNIT}};',
				],
			]
		);
		
		$this->add_responsive_control(
			'extend_all_in_one_content_main_heading_top_space',
			[
				'label' => __( 'Heading Top Spacing', 'extender-all-in-one-for-elementor' ),
				'type'  => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .extend-content-item-container .extand-content-box-heading' => 'margin-top: {{SIZE}}{{UNIT}};',
				],
			]
		);	

		$this->add_responsive_control(
			'extend_all_in_one_content_main_heading_bottom_space',
			[
				'label' => __( 'Heading Bottom Spacing', 'extender-all-in-one-for-elementor' ),
				'type'  => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .extend-content-item-container .extand-content-box-heading' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);	

		$this->add_responsive_control(
			'extend_all_in_one_content_after_heading_bottom_space',
			[
				'label' => __( 'After Heading Bottom Spacing', 'extender-all-in-one-for-elementor' ),
				'type'  => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .extend-content-item-container .extand-content-box-after-heading' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'extend_all_in_one_content_before_heading_typography',
				'label'    => __( 'Before Heading Typography', 'extender-all-in-one-for-elementor' ),
				'selector' => '{{WRAPPER}} .extend-content-item-container .extand-content-box-before-heading span',
			]
		);	

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'extend_all_in_one_content_main_heading_typography',
				'label'    => __( 'Main Heading Typography', 'extender-all-in-one-for-elementor' ),
				'selector' => '{{WRAPPER}} .extend-content-item-container .extand-content-box-heading h4',
			]
		);	

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'extend_all_in_one_content_after_heading_typography',
				'label'    => __( 'After Heading Typography', 'extender-all-in-one-for-elementor' ),
				'selector' => '{{WRAPPER}} .extend-content-item-container .extand-content-box-after-heading span',
			]
		);

		$this->start_controls_tabs( 'tabs_heading_style' );

		$this->start_controls_tab(
			'tab_heading_style_normal',
			[
				'label' => __( 'Normal', 'extender-all-in-one-for-elementor' ),
			]
		);

		$this->add_control(
			'extend_all_in_one_content_before_heading_color',
			[
				'label'     => __( 'Before Heading Color', 'extender-all-in-one-for-elementor' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .extend-content-item-container .extand-content-box-before-heading' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'extend_all_in_one_content_main_heading_color',
			[
				'label'     => __( 'Main Heading Color', 'extender-all-in-one-for-elementor' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .extend-content-item-container .extand-content-box-heading h4' => 'color: {{VALUE}};',
				],
			]
		);	

		$this->add_control(
			'extend_all_in_one_content_after_heading_color',
			[
				'label'     => __( 'After Heading Color', 'extender-all-in-one-for-elementor' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .extend-content-item-container .extand-content-box-after-heading' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'tab_heading_style_hover',
			[
				'label' => __( 'Hover', 'extender-all-in-one-for-elementor' ),
			]
		);

		$this->add_control(
			'extend_all_in_one_content_hading_before_color_hover',
			[
				'label'     => __( 'Before Heading Color', 'extender-all-in-one-for-elementor' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .extend-content-item-container:hover .extand-content-box-before-heading' => 'color: {{VALUE}};',
				],
			]
		);
			
		$this->add_control(
			'extend_all_in_one_content_main_hading_color_hover',
			[
				'label'     => __( 'Main Heading Color', 'extender-all-in-one-for-elementor' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .extend-content-item-container:hover .extand-content-box-heading h4' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'extend_all_in_one_content_hading_after_color_hover',
			[
				'label'     => __( 'After Heading Color', 'extender-all-in-one-for-elementor' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .extend-content-item-container:hover .extand-content-box-after-heading' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();

		
        $this->start_controls_section(
            'extend_all_in_one_content_description_style',
            [
                'label'     => esc_html__('Descriptions', 'extender-all-in-one-for-elementor'),
                'tab'       => Controls_Manager::TAB_STYLE,
            ]
        );

		$this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'     => 'extend_all_in_one_content_description_typography',
                'label'    => esc_html__('Typography', 'extender-all-in-one-for-elementor'),
                //'scheme'   => Schemes\Typography::TYPOGRAPHY_4,
                'selector' => '{{WRAPPER}} .extend-content-item-container .extand-content-description',
            ]
        );

        $this->add_control(
            'extend_all_in_one_content_description_color',
            [
                'label'     => esc_html__('Description Color', 'extender-all-in-one-for-elementor'),
                'type'      => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .extend-content-item-container .extand-content-description' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'extend_all_in_one_content_description_margin',
            [
                'label'      => esc_html__('Margin', 'extender-all-in-one-for-elementor'),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => ['px', '%'],
                'selectors'  => [
                    '{{WRAPPER}} .extend-content-item-container .extand-content-description' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();

		/*
		* Content inner container Styling Section
		*/
		$this->start_controls_section(
			'extend_all_in_one_content_inner_item_styles',
			[
				'label' => esc_html__( 'Inner Container', 'extender-all-in-one-for-elementor' ),
				'tab'   => Controls_Manager::TAB_STYLE
			]
		);

		$this->add_control(
			'extend_all_in_one_content_inner_item_layout',
			[
				'label' => __( 'Layout', 'extender-all-in-one-for-elementor' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'layout-1',
				'options' => [
					'layout-1'  => __( 'Layout 1', 'extender-all-in-one-for-elementor' ),
					'layout-2' => __( 'Layout 2', 'extender-all-in-one-for-elementor' ),
				],
			]
		);

		$this->add_control(
			'extend_all_in_one_content_inner_item_container_alignment',
			[
				'label'   => __( 'Alignment', 'extender-all-in-one-for-elementor' ),
				'type'    => Controls_Manager::CHOOSE,
				'toggle'  => false,
				'default' => 'eleft',
				'options' => [
					'left'   => [
						'title' => __( 'Left', 'extender-all-in-one-for-elementor' ),
						'icon'  => 'eicon-text-align-left'
					],
					'center' => [
						'title' => __( 'Center', 'extender-all-in-one-for-elementor' ),
						'icon'  => 'eicon-text-align-center'
					],
					'right'  => [
						'title' => __( 'Right', 'extender-all-in-one-for-elementor' ),
						'icon'  => 'eicon-text-align-right'
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .extend-content-grid-item .extand-content-content' => 'text-align: {{VALUE}};'
				]
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name'     => 'extend_all_in_one_content_inner_item_container_background',
				'types'    => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .extend-content-grid-item .extand-content-content'
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'            => 'extend_all_in_one_content_inner_item_container_border',
				'fields_options'  => [
                    'border'      => [
                        'default' => 'solid'
                    ],
                    'width'          => [
                        'default'    => [
							'top'    => '0',
							'right'  => '0',
							'bottom' => '0',
							'left'   => '0'
                        ]
                    ],
                    'color'       => [
                        'default' => '#e3e3e3'
                    ]
				],
				'selector'        => '{{WRAPPER}} .extend-content-grid-item .extand-content-content'
			]
		);

		$this->add_responsive_control(
			'extend_all_in_one_content_item_inner_container_radius',
			[
				'label'      => __( 'Border radius', 'extender-all-in-one-for-elementor' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'separator'  => 'before',
				'default'    => [
					'top'    => '10',
					'right'  => '10',
					'bottom' => '10',
					'left'   => '10'
				],
				'selectors'  => [
					'{{WRAPPER}} .extend-content-grid-item .extand-content-content' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
				]
			]
		);

		$this->add_responsive_control(
			'extend_all_in_one_content_item_inner_container_padding',
			[
				'label'      => __( 'Padding', 'extender-all-in-one-for-elementor' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default'    => [
					'top'    => '20',
					'right'  => '20',
					'bottom' => '20',
					'left'   => '20'
				],
				'selectors'  => [
					'{{WRAPPER}} .extend-content-grid-item .extand-content-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
				]
			]
		);

		$this->add_responsive_control(
			'extend_all_in_one_content_item_inner_container_margin',
			[
				'label'      => __( 'Margin', 'extender-all-in-one-for-elementor' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default'    => [
					'top'    => '10',
					'right'  => '10',
					'bottom' => '10',
					'left'   => '10'
				],
				'selectors'  => [
					'{{WRAPPER}} .extend-content-grid-item .extand-content-content' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
				]
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name'     => 'extend_all_in_one_content_item_inner_container_box_shadow',
				'selector' => '{{WRAPPER}} .extend-content-grid-item .extand-content-content'
			]
		);

		$this->end_controls_section();

		//Button Style
		$this->start_controls_section(
			'extend_section_style_button',
			[
				'label'     => __( 'Button', 'extender-all-in-one-for-elementor' ),
				'tab'       => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'extend_heading_footer_button',
			[
				'label' => __( 'Button', 'extender-all-in-one-for-elementor' ),
				'type'  => Controls_Manager::HEADING,
			]
		);

		$this->start_controls_tabs( 'tabs_button_style' );

		$this->start_controls_tab(
			'extend_tab_button_normal',
			[
				'label' => __( 'Normal', 'extender-all-in-one-for-elementor' ),
			]
		);

		$this->add_control(
			'extend_button_text_color',
			[
				'label'     => __( 'Text Color', 'extender-all-in-one-for-elementor' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .extend-content-grid-item .extand-content-content .content-box-button' => 'color: {{VALUE}};',
					'{{WRAPPER}} .extend-content-grid-item .extand-content-content .content-box-button svg' => 'fill: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name'     => 'extend_button_background_color',
				'types'    => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .extend-content-grid-item .extand-content-content .content-box-button',
			]
		);


		$this->add_group_control(
			Group_Control_Border::get_type(), [
				'name'        => 'extend_button_border',
				'label'       => __( 'Border', 'extender-all-in-one-for-elementor' ),
				'placeholder' => '1px',
				'default'     => '1px',
				'selector'    => '{{WRAPPER}} .extend-content-grid-item .extand-content-content .content-box-button',
			]
		);

		$this->add_responsive_control(
			'extend_button_border_radius',
			[
				'label'      => __( 'Border Radius', 'extender-all-in-one-for-elementor' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .extend-content-grid-item .extand-content-content .content-box-button' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'extend_button_text_padding',
			[
				'label'      => __( 'Padding', 'extender-all-in-one-for-elementor' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [ 
					'{{WRAPPER}} .extend-content-grid-item .extand-content-content .content-box-button' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'extend_all_in_one_content_item_inner_container_button_margin',
			[
				'label'      => __( 'Margin', 'extender-all-in-one-for-elementor' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default'    => [
					'top'    => '10',
					'right'  => '10',
					'bottom' => '10',
					'left'   => '10'
				],
				'selectors'  => [
					'{{WRAPPER}} .extend-content-grid-item .extand-content-content .content-box-button' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
				]
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name'     => 'extend_button_box_shadow',
				'selector' => '{{WRAPPER}} .extend-content-grid-item .extand-content-content .content-box-button',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'extend_button_typography',
				'label'    => __( 'Typography', 'extender-all-in-one-for-elementor' ),
				'selector' => '{{WRAPPER}} .extend-content-grid-item .extand-content-content .content-box-button',
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'extend_tab_button_hover',
			[
				'label' => __( 'Hover', 'extender-all-in-one-for-elementor' ),
			]
		);

		$this->add_control(
			'extend_button_hover_color',
			[
				'label'     => __( 'Text Color', 'extender-all-in-one-for-elementor' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .extend-content-grid-item .extand-content-content .content-box-button:hover' => 'color: {{VALUE}};',
					'{{WRAPPER}} .extend-content-grid-item .extand-content-content .content-box-button:hover svg' => 'fill: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name'     => 'extend_button_background_hover_color',
				'types'    => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .extend-content-grid-item .extand-content-content .content-box-button:hover',
			]
		);

		$this->add_control(
			'extend_button_hover_border_color',
			[
				'label'     => __( 'Border Color', 'extender-all-in-one-for-elementor' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .extend-content-grid-item .extand-content-content .content-box-button:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();

		 //Style
		 $this->start_controls_section(
            'extend_content_image_style',
            [
                'label' => __( 'Content Iamge', 'extender-all-in-one-for-elementor' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->start_controls_tabs( 'extend_content_image_style_tabs' );

        $this->start_controls_tab(
            'extend_content_image_style_normal',
            [
                'label' => __( 'Normal', 'extender-all-in-one-for-elementor' ),
            ]
        );

        $this->add_control(
            'extend_content_image_style_bg_color',
            [
                'label' => __( 'Background Color', 'extender-all-in-one-for-elementor' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .extend-content-item .extand-content-type-image' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'        => 'extend_content_image_style_border',
				'label'       => esc_html__( 'Border', 'extender-all-in-one-for-elementor' ),
				'selector'    => '{{WRAPPER}} .extend-content-item .extand-content-type-image',
			]
		);

		$this->add_control(
			'extend_content_image_style_border_radius',
			[
				'label'      => esc_html__( 'Border Radius', 'extender-all-in-one-for-elementor' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .extend-content-item .extand-content-type-image' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					'{{WRAPPER}} .extend-content-item .extand-content-type-image img' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

        $this->add_responsive_control(
            'extend_content_image_style_padding',
            [
                'label' => __( 'Padding', 'extender-all-in-one-for-elementor' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', 'em', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .extend-content-item .extand-content-type-image' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_control(
            'extend_content_image_style_image_opacity',
            [
                'label' => __( 'Opacity', 'extender-all-in-one-for-elementor' ),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'max' => 1,
                        'min' => 0.10,
                        'step' => 0.01,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .extend-content-item .extand-content-type-image img' => 'opacity: {{SIZE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Css_Filter::get_type(),
            [
                'name' => 'extend_content_image_style_image_css_filters',
                'selector' => '{{WRAPPER}} .extend-content-item .extand-content-type-image img',
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab( 'hover',
            [
                'label' => __( 'Hover', 'extender-all-in-one-for-elementor' ),
            ]
        );

        $this->add_control(
            'extend_content_image_style_bg_hover_color',
            [
                'label' => __( 'Background Color', 'extender-all-in-one-for-elementor' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .extend-content-item .extand-content-type-image' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'extend_content_image_style_border_hover_color',
            [
                'label' => __( 'Border Color', 'extender-all-in-one-for-elementor' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .extend-content-item .extand-content-type-image' => 'border-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'extend_content_image_style_image_opacity_hover',
            [
                'label' => __( 'Opacity', 'extender-all-in-one-for-elementor' ),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'max' => 1,
                        'min' => 0.10,
                        'step' => 0.01,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .extend-content-item .extand-content-type-image:hover img' => 'opacity: {{SIZE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Css_Filter::get_type(),
            [
                'name' => 'extend_content_image_style_image_css_filters_hover',
                'selector' => '{{WRAPPER}} .extend-content-item .extand-content-type-image:hover img',
            ]
        );

        $this->add_control(
            'extend_content_image_style_image_bg_hover_transition',
            [
                'label' => __( 'Transition Duration', 'extender-all-in-one-for-elementor' ),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'max' => 3,
                        'step' => 0.1,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .extend-content-item .extand-content-type-image:hover img' => 'transition-duration: {{SIZE}}s;',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->end_controls_section();

		// Carousel Navigation  Dots Settings
        $this->extender_navigation_dots_style('all-in-one');
        
		// Carousel Navigation Arrows Settings
        $this->extender_navigation_arrows_style('all-in-one');

		// Carousel Navigation Fraction Settings
        $this->extender_navigation_fraction_style('all-in-one');

		// Carousel Navigation Progressbar Settings
        $this->extender_navigation_progressbar_style('all-in-one');

    }

    protected function render() {
        $settings  = $this->get_settings_for_display();

		if ( 'grid' === $settings['extend_content_layout_type'] ) {
		
			$this->add_render_attribute('extend-content-items', 'class', 'extand-row-wrapper');
			$this->add_render_attribute('extend-content-items', 'class', 'extand-col-' . $settings['extend_columns'] );

            $this->add_render_attribute('extend-content-item', 'class', 'extend-content-grid-item extand-col');

            $this->add_render_attribute('extend-content-item-container', 'class', 'extend-content-item-container container-grid');
            $this->add_render_attribute('extend-content-item-container', 'class', $settings[ 'extend_all_in_one_content_item_layout' ]);
           
           

		}

		if ( 'carousel' === $settings['extend_content_layout_type'] ) {
			
			$this->add_render_attribute('extend-carousel-wrapper', 'class', 'extend-all-in-one-wrapper');
			$this->add_render_attribute('extend-carousel-wrapper', 'class', 'extend-content-style-type-' . $settings['extend_content_layout_type'] );
			$this->add_render_attribute('extend-content-item', 'class', 'swiper-slide');
			$this->add_render_attribute('extend-content-item', 'class', 'extend-content-item extend-content-grid-item');
			$this->add_render_attribute('extend-content-item-container', 'class', 'extend-content-item-container container-carousel');
			$this->add_render_attribute('extend-content-item-container', 'class', $settings[ 'extend_all_in_one_content_item_layout' ]);
		
		}

		?>
		<?php if ( 'carousel' === $settings['extend_content_layout_type'] ) {
			//Global Function
		$this->extender_render_global_swiper_attribute('all-in-one') ;?>

			<div <?php echo $this->get_render_attribute_string( 'extend-carousel-wrapper' ); ?>>
				<div class="swiper-container extend-all-in-one-items">
					<div class="swiper-wrapper">
				<?php }  else { ?>
				<div <?php echo $this->get_render_attribute_string( 'extend-content-items' ); ?> > 
				
				<?php } ?>
					<?php 
					//extend_all_in_one_repeater
					foreach ( $settings['extend_all_in_one_repeater'] as $index => $item ) : ?>
					
					 <?php $button_key = 'content-item-button' . $index;
					  	   $image_key = 'content-item-image' . $index; 
					  	   $content_box_image_key = 'content-box-image' . $index;
					  	   $content_box_icon_key = 'content-box-icon' . $index; ?>
						<div <?php echo $this->get_render_attribute_string( 'extend-content-item' ); ?> >
						<div <?php echo $this->get_render_attribute_string( 'extend-content-item-container' ); ?> > 
							<?php
							if ( "content" === $item['extend_all_in_one_content_type'] ) { ?>

								<div class="extand-content-type-content-box">
									<?php if ( "extend_content_image" == $item['extend_all_in_one_content_box_type'] ) { ?>  
										<div class="extand-content-image">
										<?php 
										if ( ! empty( $item['extend_all_in_one_content_icon_image_link_to']['url'] ) ) {
											$this->add_render_attribute( $content_box_image_key, 'href', $item['extend_all_in_one_content_icon_image_link_to']['url'] );

											if ( $item['extend_all_in_one_content_icon_image_link_to']['is_external'] ) {
												$this->add_render_attribute( $content_box_image_key, 'target', '_blank' );
											}

											if ( $item['extend_all_in_one_content_icon_image_link_to']['nofollow'] ) {
												$this->add_render_attribute( $content_box_image_key, 'rel', 'nofollow' );
											}
											if ( $item['extend_all_in_one_content_icon_image_link_to'] ) {
												$this->add_render_attribute( $content_box_image_key, 'class', 'extand-content-box-image-link' );
											}
										}
										if ( "yes" == $item['extend_all_in_one_content_icon_image_link_to_yes']  ){
									?>
									<a <?php echo $this->get_render_attribute_string( $content_box_image_key ); ?> > <?php } ?>
											<?php echo Group_Control_Image_Size::get_attachment_image_html( $item, 'extend_all_in_one_content_selected_image_size', 'extend_all_in_one_content_selected_image' ); ?>
										</div>
										<?php if ( "yes" == $item['extend_all_in_one_content_icon_image_link_to_yes']  ){ ?>
									</a>
									<?php } ?>
									<?php } if ( "extend_content_icon" == $item['extend_all_in_one_content_box_type'] ) { ?>
										<div class="extand-content-icon">
										<?php 
										if ( ! empty( $item['extend_all_in_one_content_icon_image_link_to']['url'] ) ) {
											$this->add_render_attribute( $content_box_icon_key, 'href', $item['extend_all_in_one_content_icon_image_link_to']['url'] );

											if ( $item['extend_all_in_one_content_icon_image_link_to']['is_external'] ) {
												$this->add_render_attribute( $content_box_icon_key, 'target', '_blank' );
											}

											if ( $item['extend_all_in_one_content_icon_image_link_to']['nofollow'] ) {
												$this->add_render_attribute( $content_box_icon_key, 'rel', 'nofollow' );
											}
											if ( $item['extend_all_in_one_content_icon_image_link_to'] ) {
												$this->add_render_attribute( $content_box_icon_key, 'class', 'extand-content-box-icon-link' );
											}
										}
										if ( "yes" == $item['extend_all_in_one_content_icon_image_link_to_yes']  ){
									?>
									<a <?php echo $this->get_render_attribute_string( $content_box_icon_key ); ?> > <?php } ?>
											<?php Icons_Manager::render_icon( $item['extend_all_in_one_content_selected_icon'] ); ?>
											<?php if ( "yes" == $item['extend_all_in_one_content_icon_image_link_to_yes']  ){ ?>
										</a>
									<?php } ?>
										</div>
									
										<?php } ?>
										<div class="extand-content-content">
										<?php if ( "yes" == $item['extend_all_in_one_content_show_before_heading'] ) { ?>
											<div class="extand-content-box-before-heading">
												<span><?php echo esc_html( $item[ 'extend_all_in_one_content_before_heading' ] );?> </span>
											</div>
										<?php } if ( "yes" == $item['extend_all_in_one_content_show_heading'] ) { ?>
											<div class="extand-content-box-heading">
												<h4><?php echo esc_html( $item[ 'extend_all_in_one_content_heading' ] );?> </h4>
											</div>
										<?php } if ( "yes" == $item['extend_all_in_one_content_show_after_heading'] ) { ?>
											<div class="extand-content-box-after-heading">
												<span><?php echo esc_html( $item[ 'extend_all_in_one_content_after_heading' ] );?> </span>
											</div>
										<?php } if ( "yes" == $item['extend_all_in_one_content_show_content'] ) { ?>
											<div class="extand-content-description">
												<?php echo $this->parse_text_editor( $item[ 'extend_all_in_one_content' ] );?>
											</div>
										<?php } if ( "yes" == $item['extend_all_in_one_content_show_box_button'] ) { 

											$this->add_render_attribute( $button_key, 'class', ['content-box-button', 'kit_extd-display-inline-block'] );
											$this->add_render_attribute( $button_key, 'class', $item['extend_all_in_one_content_show_box_button_icon_align'] );
											
											if ( ! empty( $item['extend_all_in_one_content_button_link']['url'] ) ) {
												$this->add_render_attribute( $button_key, 'href', $item['extend_all_in_one_content_button_link']['url'] );

												if ( $item['extend_all_in_one_content_button_link']['is_external'] ) {
													$this->add_render_attribute( $button_key, 'target', '_blank' );
												}

												if ( $item['extend_all_in_one_content_button_link']['nofollow'] ) {
													$this->add_render_attribute( $button_key, 'rel', 'nofollow' );
												}
												if ( $item['extend_all_in_one_content_box_button_animation'] ) {
													$this->add_render_attribute( $button_key, 'class', 'elementor-animation-' . $item['extend_all_in_one_content_box_button_animation'] );
												}
											}
											
											?>
											<a <?php echo $this->get_render_attribute_string( $button_key ); ?> ><span class="button-icon"><?php Icons_Manager::render_icon( $item['extend_all_in_one_content_show_box_button_icon'] ); ?></span><span class="button-text"><?php echo esc_html( $item[ 'extend_all_in_one_content_box_button' ] );?></span></a>
										<?php } ?>
									</div>
								</div>

							<?php 
							} elseif ( "shortcode" === $item['extend_all_in_one_content_type'] ) { ?>

								<div class="extand-content-type-shortcode">
									<?php echo do_shortcode( $item['extend_all_in_one_shortcode'] ); ?>
								</div>

							<?php
							} elseif ( "image" === $item['extend_all_in_one_content_type'] ) { ?>

								<div class="extand-content-type-image">
									<?php 
										if ( ! empty( $item['extend_all_in_one_image_link_to']['url'] ) ) {
											$this->add_render_attribute( $image_key, 'href', $item['extend_all_in_one_image_link_to']['url'] );

											if ( $item['extend_all_in_one_image_link_to']['is_external'] ) {
												$this->add_render_attribute( $image_key, 'target', '_blank' );
											}

											if ( $item['extend_all_in_one_image_link_to']['nofollow'] ) {
												$this->add_render_attribute( $image_key, 'rel', 'nofollow' );
											}
											if ( $item['extend_all_in_one_image_link_to'] ) {
												$this->add_render_attribute( $image_key, 'class', 'extand-content-type-image' );
											}
										} else if( "file" === $item['extend_all_in_one_image_link_to_type'] ) {
											$this->add_render_attribute( $image_key, 'href', $item['extend_all_in_one_image']['url'] );
											$this->add_render_attribute( $image_key, 'class', 'extand-content-type-image-lightbox' );
											$this->add_render_attribute( $image_key, 'data-elementor-open-lightbox', 'yes' );
										}
										if( ! empty( $item['extend_all_in_one_image_link_to_type'] ) ){
									?>
									<a <?php echo $this->get_render_attribute_string( $image_key ); ?> > <?php } ?>
									<?php echo Group_Control_Image_Size::get_attachment_image_html( $item, 'extend_all_in_one_image_size', 'extend_all_in_one_image' ); ?>
									<?php if( ! empty( $item['extend_all_in_one_image_link_to_type'] ) ){ ?>
									</a>
									<?php } ?>
								</div>

							<?php
							} else {

								echo Plugin::$instance->frontend->get_builder_content_for_display( wp_kses_post( $item['extend_all_in_one_save_template'] ) );

							} ?>
						</div>
						</div>
					<?php
					endforeach;
					?>
		<?php if ( 'carousel' === $settings['extend_content_layout_type'] ) { ?>
				</div>
			</div>
			<?php if( $settings['extend_carousel_nav'] === "arrows" || $settings['extend_carousel_nav'] === "arrows-dots" || $settings['extend_carousel_nav'] === "arrows-progress-bar" || $settings['extend_carousel_nav'] == "arrows-fraction" ) : ?>
                            <div class="extand-carousel-nav-next"><i class="eicon-chevron-right"></i></div>
                            <div class="extand-carousel-nav-prev"><i class="eicon-chevron-left"></i></div>
                        <?php endif; ?>
                        <?php if( $settings['extend_carousel_nav'] === "nav-dots" || $settings['extend_carousel_nav'] === "arrows-dots" || $settings['extend_carousel_nav'] === "progress-bar" || $settings['extend_carousel_nav'] === "fraction" || $settings['extend_carousel_nav'] === "arrows-progress-bar" || $settings['extend_carousel_nav'] === "arrows-fraction" ) : ?>
                            <div class="extand-dots-container">
                                <div class="extand-swiper-pagination swiper-pagination"></div>
                            </div>
                        <?php endif; ?>
		</div>
		<?php } else { ?>
		</div>
		<?php } ?>
		<?php

    }
}

Plugin::instance()->widgets_manager->register( new All_In_One() );